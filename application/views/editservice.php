<div class="row">
    <div class="col-lg-12">
        <h1>Edit Service</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Please make sure that the details below are correct and up to date before adding the service. 
        </div>
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <section class="container">
            <div class="container-page">	
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-rss"></i> Details required to edit service</h3>
                    </div>
                    <div class="panel-body feed">
                        <?php if ($this->session->flashdata('error_message')) { ?>
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('error_message'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success_message')) { ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('success_message'); ?>
                            </div>
                        <?php } ?>
                        <form role="form" action="<?php echo base_url(); ?>appointment/edit_service/<?php echo $service_info['id'];?>" method="post" >
                            <div class="col-md-6">
                                <h3 class="dark-grey">Service Details</h3>
                                <div class="form-group col-lg-6" >
                                    <label>Gender</label>
                                    <select class="form-control" name="gender" >
                                        <option value="m" <?php if ($service_info['gender'] == 'm') echo 'selected'; ?> >Male</option>
                                        <option value="f" <?php if ($service_info['gender'] == 'f') echo 'selected'; ?>>Female</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Category</label>
                                    <select class="form-control" name="category" >
                                        <option value=""></option>
                                        <?php foreach ($categories as $category) { ?>
                                            <option value="<?php echo $category['id']; ?>" <?php if ($category['id'] == $service_info['category_id']) echo 'selected'; ?> ><?php echo $category['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Sub Category</label>
                                    <select class="form-control" name="subcategory" >
                                        <option value=""></option>
                                        <?php foreach ($subcategories as $subcategory) { ?>
                                            <option value="<?php echo $subcategory['id']; ?>" <?php if ($subcategory['id'] == $service_info['subcategory_id']) echo 'selected'; ?> ><?php echo $subcategory['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6" >
                                    <label>Service Name</label>
                                    <input type="text" name="service_name" class="form-control"  value="<?php echo $service_info['name']; ?>">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Duration in Chair</label>
                                    <div class="form-group col-lg-6">
                                        <input type="text" name="duration_in_hours" class="form-control"  value="<?php echo $service_info['duration_in_hours']; ?>">Hr
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="text" name="duration_in_mins" class="form-control"  value="<?php echo $service_info['duration_in_mins']; ?>">min
                                    </div>

                                </div>	
                                <div class="form-group col-lg-6">
                                    <label>Duration out of Chair</label>
                                    <div class="form-group col-lg-6">
                                        <input type="text" name="duration_out_hours" class="form-control"  value="<?php echo $service_info['duration_out_hours']; ?>">Hr
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="text" name="duration_out_mins" class="form-control"  value="<?php echo $service_info['duration_out_mins']; ?>">min
                                    </div>

                                </div>	
                                <div class="form-group col-lg-6">
                                    <label>Cost of Service</label>
                                    <input type="text" name="cost" class="form-control"  value="<?php echo $service_info['cost']; ?>" >
                                </div>	
                                <div class="col-md-6"><br>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Edit Service</button>			</div>							
                            </div>		
                        </form>    
                        <br>

                        <div class="col-md-6">

                            <h3 class="dark-grey">Easy as 1 - 2 - 3</h3>
                            <p>
                                Information can be displayed here on how to work the system
                            </p>
                            <p>
                                Information can be displayed here on how to work the system Information can be displayed here on how to work the system- 
                                Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                            </p>
                            <p>
                                Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                            </p>
                            <p>
                                Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                            </p>
                            <br>

                        </div>
                    </div>
                    </section>
                </div>
            </div>
    </div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
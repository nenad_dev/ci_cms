<div class="row">
    <div class="col-lg-12">
        <h1>List of Services</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your list of Services Page. 
        </div>
    </div>
    <?php if ($this->session->flashdata('error_message')) { ?>
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('success_message')) { ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php } ?>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Services list</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Service Name</th>
                                <th>Gender</th>
                                <th>Duration</td>
                                <th>Price</td>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($services as $service) { ?>
                                <tr>
                                    <td><?php echo $service['category']; ?></td>
                                    <td><?php echo $service['subcategory']; ?></td>
                                    <td><?php echo $service['name']; ?></td>
                                    <td><?php
                            if ($service['gender'] == 'm')
                                echo 'Male';
                            else
                                echo 'Female';
                                ?></td>
                                    <td><?php echo $service['duration_hours']; ?>h <?php echo $service['duration_mins']; ?>mins</td>
                                    <td><?php echo $service['cost']; ?></td>
                                    <td><a href="<?php echo base_url(); ?>dashboard/editservice/<?php echo $service['id']; ?>" >Edit</a></td>
                                    <td><a href="<?php echo base_url(); ?>appointment/delete_service/<?php echo $service['id']; ?>" >X</a></td>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
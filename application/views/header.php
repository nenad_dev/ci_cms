<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $page_title; ?></title>
        <?php echo $css; ?>
        <?php echo $js_scripts; ?>
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">Your Company Logo</a>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul id="active" class="nav navbar-nav side-nav">
                        <li class="selected"><a href="<?php echo base_url(); ?>"><i class="fa fa-bullseye"></i> Home</a></li>
                        <?php if ($role == 1) { ?>
                            <li><a href="#"></a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/addcategory"><i class="fa fa-list-ol"></i> Add Category</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/viewcategories"><i class="fa fa-list-ol"></i> View Categories</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/addservice"><i class="fa fa-list-ol"></i> Add Service</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/viewservices"><i class="fa fa-list-ol"></i> View Services</a></li>
                        <?php } ?>

                        <?php if (($role == 1) || ($role == 2)) { ?>
                            <li><a href="<?php echo base_url(); ?>dashboard/viewbookings"><i class="fa fa-list-ol"></i> View Bookings</a></li>
                        <?php } ?>
                        <?php if ($role == 1) { ?>
                            <li><a href="#"></a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/addadmin"><i class="fa fa-list-ol"></i> Add Admin</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/viewadmins"><i class="fa fa-list-ol"></i> View Admins</a></li>
                        <?php } ?>
                        <?php if ($role == 1) { ?>    
                            <li><a href="#"></a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/addstylist"><i class="fa fa-list-ol"></i> Add Stylist</a></li>
                        <?php } ?>    
                        <li><a href="<?php echo base_url(); ?>dashboard/viewstylists"><i class="fa fa-list-ol"></i> View Stylists</a></li>
                        <?php if ($role != 4) { ?>
                            <li><a href="#"></a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/addclient"><i class="fa fa-list-ol"></i> Add Client</a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/viewclients"><i class="fa fa-list-ol"></i> View Clients</a></li>
                        <?php } ?>

                        <li><a href="#"></a></li>
                        <li><a href="<?php echo base_url(); ?>dashboard/bookappointment"><i class="fa fa-globe"></i> Book Appointment</a></li>
                        <?php if (($role == 1) || ($role == 2)) { ?>
                            <li><a href="applist.php"><i class="fa fa-list-ol"></i> View All Appointments</a></li>
                        <?php } ?>
                        <li><a href="<?php echo base_url(); ?>dashboard/appointmenthistory"><i class="fa fa-list-ol"></i> Appointment History</a></li>
                        <?php if ($role == 1) { ?>    
                            <li><a href="<?php echo base_url(); ?>dashboard/moveclients"><i class="fa fa-list-ol"></i> Move Clients</a></li>
                        <?php } ?>
                        <li><a href="#"></a></li>
                        <li><a href="<?php echo base_url(); ?>user/edit"><i class="fa fa-font"></i> Edit Profile</a></li>
                        <?php if ($role != 1) { ?>
                            <li><a href="#"></a></li>
                            <li><a href="<?php echo base_url(); ?>dashboard/contact"><i class="fa fa-font"></i>Contact us</a></li>
                        <?php } ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right navbar-user">
                        <li class="dropdown user-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Welcome Back, <?php echo $this->session->userdata('user_full_name'); ?>!<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>user/profile"><i class="fa fa-user"></i> Profile</a></li>
                                <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>user/logout"><i class="fa fa-power-off"></i> Log Out</a></li>

                            </ul>
                        </li>
                        <li class="divider-vertical"></li>
                        <li>
                            <form class="navbar-search">
                                <input type="text" placeholder="Search" class="form-control">
                            </form>
                        </li>
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper">
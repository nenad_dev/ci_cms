<div class="row">
    <div class="col-lg-12">
        <h1>Stylists Dashboard</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your Profile Page! Customize your Profile, Book your appointments and view your previous visits. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Stylist Calendar</h3></div>
            <div class="panel-body feed">
                <div id="datepicker_cont">
                    <label>Please select day</label>
                    <input type="text" id="stylist_calendar_datepicker" value="<?php echo date('Y-m-d'); ?>"/>
                </div>
                <div id="stylist_calendar">
                    <?php echo $stylist_calendar;?>
                </div>
            </div>
        </div> 
    </div>
</div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
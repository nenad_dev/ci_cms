<div class="row">
    <div class="col-lg-12">
        <h1>Add Category</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">×</button>
            Please make sure that the details below are correct and up to date before adding a Category. 
        </div>
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <section class="container">
            <div class="container-page">	
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-rss"></i> Details required to add a new Category</h3>
                    </div>
                    <div class="panel-body feed">
                        <?php if ($this->session->flashdata('error_message')) { ?>
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('error_message'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success_message')) { ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('success_message'); ?>
                            </div>
                        <?php } ?>
                        <form role="form" action="<?php echo base_url(); ?>appointment/edit_category/<?php echo $category_info['id'];?>" method="post" >
                            <div class="col-md-6">	
                                <div class="col-md-12">
                                    <h3 class="dark-grey">Category Details</h3>
                                </div>			
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <label>Category Name</label>
                                        <input type="text" name="category_name" class="form-control" id="category_name" value="<?php echo $category_info['name']; ?>">
                                    </div>
                                    <div class="col-md-6"><br>

                                    </div>
                                </div>	
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Edit Category</button>
                                </div>	
                            </div>
                        </form>    
                        <div class="col-md-6">

                            <h3 class="dark-grey">Easy as 1 - 2 - 3</h3>
                            <p>
                                Information can be displayed here on how to work the system
                            </p>
                            <p>
                                Information can be displayed here on how to work the system Information can be displayed here on how to work the system- 
                                Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                            </p>
                            <p>
                                Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                            </p>
                            <p>
                                Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                            </p>
                            <br>
                        </div>
                    </div>		
                </div>
            </div></section>
    </div>
</div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
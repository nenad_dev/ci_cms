<div class="row">
    <div class="col-lg-12">
        <h1>Edit Profile</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your Profile Page! Customize your Profile, Book your appointments and view your previous visits. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Profile Information</h3>
            </div>
            <div class="panel-body feed">
                <?php if ($this->session->flashdata('success_message')) { ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('error_message')) { ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php } ?>
                <form role="form" action="<?php echo base_url(); ?>user/edit_user/<?php echo $this->uri->segment(3); ?>" method="post" >
                    <?php if (isset($stylist_work_days)) { ?>
                        <div class="panel-body feed">
                            <?php echo $stylist_work_days; ?>
                        </div>
                    <?php } ?>
                    <div class="form-group col-lg-6">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control" id="first_name" value="<?php echo $profile['first_name']; ?>">
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control" id="last_name" value="<?php echo $profile['last_name']; ?>">
                    </div>
                    <div class="form-group col-lg-12">
                        <label>Email Address</label>
                        <input type="email" name="email" class="form-control" id="email" value="<?php echo $profile['email']; ?>">
                    </div>
                    <div class="form-group col-lg-12">
                        <label>Contact Number</label>
                        <input type="phone" name="phone" class="form-control" id="phone" value="<?php echo $profile['phone']; ?>">
                    </div>
                    <?php if (($profile['role'] == 4) && ((($this->uri->segment(3) == '') && $role == 4) || ((($role == 1) || ($role == 2)) && ($this->uri->segment(3) != '')))) { ?>
                        <div class="form-group col-lg-12">
                            <label>Stylist of Choice</label>
                            <select name="stylist" class="form-control" >
                                <?php foreach ($stylists as $stylist) { ?>
                                    <option value="<?php echo $stylist['id']; ?>" <?php if ($stylist['id'] == $stylist_id) echo 'selected'; ?> ><?php echo $stylist['first_name'] . ' ' . $stylist['last_name']; ?></option>
                                <?php } ?>
                            </select>    
                        </div>
                    <?php } ?>
                    <?php if (($profile['role'] == 4) && (($role == 1) || ($role == 2))) { ?>
                        <div class="form-group col-lg-12">
                            <label>Status</label>
                            <select name="status" class="form-control" >
                                <option value="0" <?php if ($profile['status'] == 0) echo 'selected'; ?> >Approval required</option>
                                <option value="1" <?php if ($profile['status'] == 1) echo 'selected'; ?> >Approved</option>
                                <option value="2" <?php if ($profile['status'] == 2) echo 'selected'; ?> >Denied</option>
                            </select>    
                        </div>
                    <?php } ?>

                    <div class="form-group col-lg-6">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" id="password" value="">
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Repeat Password</label>
                        <input type="password" name="confirm_password" class="form-control" id="confirm_password" value="">
                    </div>
                    <div class="form-group col-lg-6">
                        <button type="submit" class="btn btn-primary">Update Profile</button>
                    </div>
                </form>            
            </div>
        </div>
    </div>              
</div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
var appointment_date;
$(function () {

    $('#bookappointment #category').change(function () {
        $('#bookappointment #subcategory_cont').hide();
        $('#bookappointment #service_cont').hide();
        if ($(this).val() != '') {
            get_subcategories('bookappointment');
        }
    });

    $('#addservice_cont #category').change(function () {
        get_subcategories('addservice');
    });

    $('#bookappointment #subcategory').change(function () {
        $('#bookappointment #service_cont').hide();
        get_services();

    });

    $('#bookappointment_proceed_1').click(function () {
        if (($('#bookappointment #service option').size() > 0) && $('#bookappointment #service').val() != '') {
            get_appointment_availability_content();
            $('#step1').hide();
            $('#step2').show();
        } else {
            alert('Please choose Service');
        }
    });

    $('#bookappointment_back_1').click(function () {
        $('#step1').show();
        $('#step2').hide();
    });


    $('#datepicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).val();

    $('#datepicker').change(function () {
        get_appointment_availability_content();
    });

    $('body').on('click', '#appointments .avaliable a', function () {
        $(this).parent('td').prev().addClass('selected_time');
        $('#category_chosen').text($('#category option:selected').text());
        $('#subcategory_chosen').text($('#subcategory option:selected').text());
        $('#service_chosen').text($('#service option:selected').text());
        $('#stylist_chosen').text($('#stylist').text());

        var selected_date = $(this).parents('td').find('.date').text();
        var selected_time = $(this).parents('tr').find('.time').text();

        appointment_date = selected_date + ' ' + selected_time + ':00';
        $('#timeslot_chosen').text(appointment_date);

        $('#step2').hide();
        $('#step3').show();

        return false;
    });

    $('body').on('click', '#confirm_appointment', function () {
        var service = $('#service').val();
        $.ajax({
            url: base_url + 'appointment/ajax_set_appointment',
            type: 'POST',
            data: {
                'date': appointment_date,
                'service': service,
                'ajax': 1
            },
            async: false,
            success: function (response) {
                if (response == 'ok') {
                    $('#bookappointment_message').html('Appointment successfully booked!');
                    $('#bookappointment_message').addClass('alert-success');
                } else {
                    $('#bookappointment_message').html('Some problem appeared!');
                    $('#bookappointment_message').addClass('alert-danger');
                }
                $('#confirm_appointment').hide();
                $('#bookappointment_message').show();
            }
        });

        return false;
    });

    $('#move_clients #client').change(function () {
        var current_val = $(this).val();
        if (current_val == 'all') {
            $('#from_stylist_cont').show();
        } else {
            $('#from_stylist_cont').hide();
        }
    });

    // stylist calendar
    $('#stylist_calendar_datepicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).val();

    $('#stylist_calendar_datepicker').change(function () {
        var date = $('#stylist_calendar_datepicker').val();
        $.ajax({
            url: base_url + 'appointment/ajax_get_stylist_calendar',
            type: 'POST',
            data: {
                "date": date,
                "ajax": 1
            },
            async: false,
            success: function (response) {
                $('#stylist_calendar').html(response);
            }
        });
    });


});

function get_appointment_availability_content() {
    $('#appointment_availability_content').html('');
    var service = $('#service').val();
    var date = $('#datepicker').val();
    $.ajax({
        url: base_url + 'appointment/ajax_get_appointment_availability_content',
        type: 'POST',
        data: {
            "date": date,
            "service": service,
            "ajax": 1
        },
        async: false,
        success: function (response) {
            $('#appointment_availability_content').html(response);
        }
    });
}

function get_subcategories(which) {
    var id = $('#category').val();
    $.ajax({
        url: base_url + 'appointment/ajax_get_subcategories_for_category',
        type: 'POST',
        data: {
            'id': id,
            'ajax': 1
        },
        async: false,
        success: function (response) {
            var obj = $.parseJSON(response);
            var option;
            if (obj.status == 'ok') {
                $('#subcategory').html('');
                $('#subcategory').append('<option value=""></option>');
                if (which == 'addservice') {
                    $('#subcategory').append('<option value="0">None</option>');
                }
                $.each(obj.items, function (index, value) {
                    option = '<option value="' + value['id'] + '" >' + value['name'] + '</option>';
                    $('#subcategory').append(option);
                });

                if (which == 'bookappointment') {
                    if (obj.items.length > 0) {
                        $('#subcategory_cont').show();
                    } else {
                        $('#bookappointment #subcategory').change();
                        $('#bookappointment #subcategory option:last-child').attr('selected', 'selected');
                    }
                }
            }
        }
    });
}

function get_services() {
    var subcategory_id = $('#bookappointment #subcategory').val();
    var category_id = $('#bookappointment #category').val();
    $.ajax({
        url: base_url + 'appointment/ajax_get_services_for_subcategory',
        type: 'POST',
        data: {
            'subcategory_id': subcategory_id,
            'category_id': category_id,
            'ajax': 1
        },
        async: false,
        success: function (response) {
            var obj = $.parseJSON(response);
            var option;
            if (obj.status == 'ok') {
                $('#bookappointment #service').html('');
                $('#bookappointment #service').append('<option value=""></option>');
                $.each(obj.items, function (index, value) {
                    option = '<option value="' + value['id'] + '" >' + value['name'] + '</option>';
                    $('#bookappointment #service').append(option);
                });

                $('#bookappointment #service_cont').show();
            }
        }
    });
}
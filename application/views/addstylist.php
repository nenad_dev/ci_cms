<div class="row">
    <div class="col-lg-12">
        <h1>Add Stylist</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Please make sure that the details below are correct and up to date before adding the Stylist user. 
        </div>
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <section class="container">
            <div class="container-page">	
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-rss"></i> Details required to add Stylist user</h3>
                    </div>
                    <div class="panel-body feed">
                        <?php if ($this->session->flashdata('error_message')) { ?>
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('error_message'); ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success_message')) { ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('success_message'); ?>
                            </div>
                        <?php } ?>
                        <form role="form" action="<?php echo base_url(); ?>user/add_user" method="post" >    
                            <?php if (isset($stylist_work_days)) { ?>
                                <div class="panel-body feed">
                                    <?php echo $stylist_work_days; ?>
                                </div>
                            <?php } ?>
                            <div class="col-md-6">
                                <h3 class="dark-grey">Stylist Details</h3>

                                <div class="form-group col-lg-6">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" class="form-control" id="first_name" value="">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" class="form-control" id="last_name" value="">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Email Address</label>
                                    <input type="email" name="email" class="form-control" id="email" value="">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label>Contact Number</label>
                                    <input type="phone" name="phone" class="form-control" id="phone" value="">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" id="password" value="">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Repeat Password</label>
                                    <input type="password" name="confirm_password" class="form-control" id="confirm_password" value="">
                                </div>
                                <input type="hidden" name="role" value="3" />
                            </div>
                            <br>
                            <div class="col-md-6">

                                <h3 class="dark-grey">Easy as 1 - 2 - 3</h3>
                                <p>
                                    Information can be displayed here on how to work the system
                                </p>
                                <p>
                                    Information can be displayed here on how to work the system Information can be displayed here on how to work the system- 
                                    Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                                </p>
                                <p>
                                    Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                                </p>
                                <p>
                                    Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                                </p>
                                <br>
                                <button type="submit" class="btn btn-primary">Add Stylist</button>
                            </div>
                        </form>
                    </div>
                    </section>
                </div>
            </div>
    </div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
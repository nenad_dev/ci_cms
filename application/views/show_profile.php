<div class="row">
    <div class="col-lg-12">
        <h1>Edit Profile</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your Profile Page! Customize your Profile, Book your appointments and view your previous visits. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Profile Information</h3>
            </div>
            <div class="panel-body feed">
                <section class="feed-item">
                    <div class="icon pull-left">
                        <i class="fa fa-comment"></i>
                    </div>
                    <div class="feed-item-body">
                        <div class="text">
                            <a href="#"><?php echo $profile['first_name'] . ' ' . $profile['last_name'];?> </a>
                        </div>
                    </div>
                </section>
                <section class="feed-item">
                    <div class="icon pull-left">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="feed-item-body">
                        <div class="text">
                            <a href="#"><?php echo $profile['phone'];?></a>
                        </div>

                    </div>
                </section>
                <section class="feed-item">
                    <div class="icon pull-left">
                        <i class="fa fa-plus-square-o"></i>
                    </div>
                    <div class="feed-item-body">
                        <div class="text">
                            <a href="#"><?php echo $profile['email'];?></a>
                        </div>

                    </div>
                </section>
                <section class="feed-item">
                    <div class="icon pull-left">
                        <i class="fa fa-bolt"></i>
                    </div>
                    <div class="feed-item-body">
                        <div class="text">
                            Pictures to show here
                        </div>
                        <div class="time pull-left">
                            Date of picture
                        </div>
                    </div>
                </section>
                <section class="feed-item">
                    <div class="icon pull-left">
                        <i class="fa fa-archive"></i>
                    </div>
                    <div class="feed-item-body">
                        <div class="text">
                            <a href="#">Stylist Name</a>.
                        </div>
                    </div>
                </section>
                <section class="feed-item">
                    <div class="icon pull-left">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="feed-item-body">
                        <div class="text">
                            <a href="#">Next Sheduled Appointment Date</a>
                        </div>
                        <div class="time pull-left">
                            Wednesday, 21 August 2015 at 3:30pm
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <a href="<?php echo base_url(); ?>user/edit/<?php echo $this->uri->segment(3); ?>" class="btn btn-default btn-lg">Edit Profile</a>
    </div>              
</div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
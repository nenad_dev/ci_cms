<?php

/* ------  User roles --------- */
/*
  /*    1 - Owner
  /*    2 - Admin
  /*    3 - Stylist
  /*    4 - Client
  /*
  /*----------------------------- */

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_appointments() {
        $this->db->select('*');
        $this->db->from('appointment');
        $result = $this->db->get();

        return $result->result_array();
    }

    function set_add_user($first_name, $last_name, $password, $email, $phone, $role, $stylist_id = '') {

        // encrypt password
        $encrypted_password = $this->_encrypt_password($password);

        $this->db->select('id');
        $this->db->from('user');
        $this->db->where('email', $email);
        $result_email = $this->db->get();

        $date_created = date('Y-m-d H:i:s');
        $date_modified = date('Y-m-d H:i:s');

        // if already exist some type of profile for user with this email
        if ($result_email->num_rows() == 1) {
            $user_id = '';
        } else {
            $user_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'password' => $encrypted_password,
                'phone' => $phone,
                'role' => $role,
                'status' => '0',
                'date_created' => $date_created,
                'date_modified' => $date_modified
            );

            $this->db->insert('user', $user_data);
            $user_id = $this->db->insert_id();

            if ($stylist_id && $user_id) {
                $client_stylist_data = array(
                    'client_id' => $user_id,
                    'stylist_id' => $stylist_id
                );

                $this->db->insert('client_stylist', $client_stylist_data);
            }
        }

        return $user_id;
    }

    function set_edit_user($first_name, $last_name, $password, $email, $phone, $id, $status, $stylist_id = '') {

        // encrypt password
        $encrypted_password = $this->_encrypt_password($password);

        $date_modified = date('Y-m-d H:i:s');

        $user_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone,
            'date_modified' => $date_modified
        );

        if ($password)
            $user_data['password'] = $encrypted_password;

        if ($status !== '')
            $user_data['status'] = $status;

        $this->db->where('id', $id);
        $this->db->update('user', $user_data);

        // update stylist id for client with id $id
        if ($stylist_id) {
            $client_stylist_data = array(
                'stylist_id' => $stylist_id
            );

            $this->db->where('client_id', $id);
            $this->db->update('client_stylist', $client_stylist_data);
        }
    }

    function set_delete_user($id) {
        $this->db->where('id', $id);
        $this->db->delete('user');
        return $this->db->affected_rows();
    }

    function get_login_user($username, $password) {

        // encrypt password
        $encrypted_password = $this->_encrypt_password($password);

        $this->db->select('id,role,first_name,last_name');
        $this->db->from('user');
        $this->db->where('email', $username);
        $this->db->where('password', $encrypted_password);
        $result = $this->db->get();

        // if already exist some type of profile for user with this email
        $user_id = '';
        if ($result->num_rows() == 1) {
            $id = $result->row(0)->id;
            $role = $result->row(0)->role;
            $user_full_name = $result->row(0)->first_name . ' ' . $result->row(0)->last_name;

            $this->session->set_userdata('logged_in', 'yes');
            $this->session->set_userdata('user_role', $role);
            $this->session->set_userdata('user_id', $id);
            $this->session->set_userdata('user_full_name', $user_full_name);

            return TRUE;
        }

        return FALSE;
    }

    function get_users($role, $limit, $select = '') {
        if ($select) {
            $this->db->select($select);
        } else {
            $this->db->select('*');
        }
        $this->db->from('user');
        $this->db->where('role', $role);
        if ($limit) {
            $this->db->limit($limit);
        }
        $result = $this->db->get();

        /*
          echo "<pre>";
          print_r($this->db->last_query()); */

        return $result->result_array();
    }

    function get_profile_info($id) {

        if ($id == '')
            $id = $this->session->userdata('user_id');

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id', $id);
        $result = $this->db->get();
        $result_array = $result->result_array();
        return $result_array[0];
    }

    function get_user_role($id) {
        $this->db->select('role');
        $this->db->from('user');
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->row(0)->role;
    }

    function get_stylist_id($client_id) {

        $stylist_id = '';
        $this->db->select('stylist_id');
        $this->db->from('client_stylist');
        $this->db->where('client_id', $client_id);
        $result = $this->db->get();
        if ($result->num_rows() == 1) {
            $stylist_id = $result->row(0)->stylist_id;
        }

        return $stylist_id;
    }

    function get_client_info($id) {
        $client = $this->get_profile_info($id);
        $stylist_id = $this->get_stylist_id($id);
        $stylist = $this->get_profile_info($stylist_id);
        $client['full_name'] = $client['first_name'] . ' ' . $client['last_name'];
        $client["stylist"] = $stylist['first_name'] . ' ' . $stylist['last_name'];
        $client["stylist_id"] = $stylist_id;
        return $client;
    }

    function _encrypt_password($password) {
        return sha1($password);
    }

    function set_move_clients($client, $from_stylist, $to_stylist) {

        $client_stylist_data = array(
            'stylist_id' => $to_stylist
        );

        if ($client == 'all') {
            $this->db->where('stylist_id', $from_stylist);
        } else {
            $this->db->where('client_id', $client);
        }

        $this->db->update('client_stylist', $client_stylist_data);
    }

    function get_user_has_clients($id) {
        $this->db->select('stylist_id');
        $this->db->from('client_stylist');
        $this->db->where('stylist_id', $id);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_stylist_work_days($stylist_id) {

        $this->db->select('*');
        $this->db->from('stylist_work_days');
        $this->db->where('stylist_id', $stylist_id);
        $result = $this->db->get();
        $result_array = $result->result_array();

        $swd = array();
        foreach ($result_array as $item) {
            $day = $item['day'];
            if (isset($item['start'])) {
                $swd[$day]['start'] = $item['start'];
            } else {
                $swd[$day]['start'] = '00:00';
            }
            if (isset($item['end'])) {
                $swd[$day]['end'] = $item['end'];
            } else {
                $swd[$day]['end'] = '00:00';
            }
        }

        $swd_info = array(
            'mon_start_time' => $swd['mon']['start'],
            'tue_start_time' => $swd['tue']['start'],
            'wed_start_time' => $swd['wed']['start'],
            'thu_start_time' => $swd['thu']['start'],
            'fri_start_time' => $swd['fri']['start'],
            'sat_start_time' => $swd['sat']['start'],
            'sun_start_time' => $swd['sun']['start'],
            'mon_end_time' => $swd['mon']['end'],
            'tue_end_time' => $swd['tue']['end'],
            'wed_end_time' => $swd['wed']['end'],
            'thu_end_time' => $swd['thu']['end'],
            'fri_end_time' => $swd['fri']['end'],
            'sat_end_time' => $swd['sat']['end'],
            'sun_end_time' => $swd['sun']['end'],
        );

        return $swd_info;
    }

    function set_insert_stylist_work_days($stylist_id, $mon_start_time, $mon_end_time, $tue_start_time, $tue_end_time, $wed_start_time, $wed_end_time, $thu_start_time, $thu_end_time, $fri_start_time, $fri_end_time, $sat_start_time, $sat_end_time, $sun_start_time, $sun_end_time) {
        $data = array(
            array(
                'stylist_id' => $stylist_id,
                'day' => 'mon',
                'start' => $mon_start_time,
                'end' => $mon_end_time
            ),
            array(
                'stylist_id' => $stylist_id,
                'day' => 'tue',
                'start' => $tue_start_time,
                'end' => $tue_end_time
            ),
            array(
                'stylist_id' => $stylist_id,
                'day' => 'wed',
                'start' => $wed_start_time,
                'end' => $wed_end_time
            ),
            array(
                'stylist_id' => $stylist_id,
                'day' => 'thu',
                'start' => $thu_start_time,
                'end' => $thu_end_time
            ),
            array(
                'stylist_id' => $stylist_id,
                'day' => 'fri',
                'start' => $fri_start_time,
                'end' => $fri_end_time
            ),
            array(
                'stylist_id' => $stylist_id,
                'day' => 'sat',
                'start' => $sat_start_time,
                'end' => $sat_end_time
            ),
            array(
                'stylist_id' => $stylist_id,
                'day' => 'sun',
                'start' => $sun_start_time,
                'end' => $sun_end_time
            )
        );

        $this->db->insert_batch('stylist_work_days', $data);
    }

    function set_update_stylist_work_days($stylist_id, $mon_start_time, $mon_end_time, $tue_start_time, $tue_end_time, $wed_start_time, $wed_end_time, $thu_start_time, $thu_end_time, $fri_start_time, $fri_end_time, $sat_start_time, $sat_end_time, $sun_start_time, $sun_end_time) {
        $data = array();
        $data['mon'] = array(
            'start' => $mon_start_time,
            'end' => $mon_end_time
        );
        $data['tue'] = array(
            'start' => $tue_start_time,
            'end' => $tue_end_time
        );
        $data['wed'] = array(
            'start' => $wed_start_time,
            'end' => $wed_end_time
        );
        $data['thu'] = array(
            'start' => $thu_start_time,
            'end' => $thu_end_time
        );
        $data['fri'] = array(
            'start' => $fri_start_time,
            'end' => $fri_end_time
        );
        $data['sat'] = array(
            'start' => $sat_start_time,
            'end' => $sat_end_time
        );
        $data['sun'] = array(
            'start' => $sun_start_time,
            'end' => $sun_end_time
        );

        foreach ($data as $key => $value) {
            $update_data = array(
                'start' => $value['start'],
                'end' => $value['end'],
            );
            $this->db->where('day', $key);
            $this->db->where('stylist_id', $stylist_id);
            $this->db->update('stylist_work_days', $update_data);
        }
    }

}

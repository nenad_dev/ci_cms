<div class="row">
    <div class="col-lg-12">
        <h1>Get in touch</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Contact us direct using our easy online contact form. 
            <br />
            We look forward to hearing from you!
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Profile Information</h3>
            </div>
            <div class="panel-body feed">
                <div class="col-md-4">
                    <img style="position:absolute;top:35px;" src="<?php echo base_url(); ?>assets/images/mail.png"/>
                </div>
                <div class="col-md-8">       
                    <form role="form">
                        <div class="form-group input-group">
                            <span class="input-group-addon"> </span>
                            <input type="text" class="form-control" placeholder="Full Name">
                            <input type="text" class="form-control" placeholder="Email Address">
                            <input type="text" class="form-control" placeholder="Contact Number">
                            <select class="form-control">
                                <option>None</option>
                                <option>Stylist 1</option>
                                <option>Stylist 2</option>
                                <option>Stylist 3</option>
                                <option>Stylist 4</option>
                                <option>Stylist 5</option>
                            </select>
                            <textarea class="form-control" rows="3"placeholder="Question or Comments"></textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <button type="button" class="btn btn-default btn-lg">Submit</button>
    </div>
</div>
<!-- /#wrapper -->
</body>
</html>
<div class="row">
    <div class="col-lg-12">
        <h1>List of Categories</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">×</button>
            Welcome to your list of Categories Page. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <?php if ($this->session->flashdata('error_message')) { ?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error_message'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success_message')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success_message'); ?>
            </div>
        <?php } ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Category list</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($categories as $category) { ?>
                                <tr>
                                    <td><?php echo $category['name']; ?></td>
                                    <td><a href="<?php echo base_url(); ?>dashboard/editcategory/<?php echo $category['id']; ?>" >Edit</a></td>
                                    <td><a href="<?php echo base_url(); ?>appointment/delete_category/<?php echo $category['id']; ?>" >X</a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Sub Category list</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Category Name</th>
                                <th>Sub Category Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($subcategories as $subcategory) { ?>
                                <tr>
                                    <td><?php echo $subcategory['main_cat_name']; ?></td>
                                    <td><?php echo $subcategory['name']; ?></td>
                                    <td><a href="<?php echo base_url(); ?>dashboard/editcategory/<?php echo $subcategory['id']; ?>" >Edit</a></td>
                                    <td><a href="<?php echo base_url(); ?>appointment/delete_category/<?php echo $subcategory['id']; ?>" >X</a></td>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
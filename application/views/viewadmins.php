<div class="row">
    <div class="col-lg-12">
        <h1>List of Admins</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your list of Admins Page. 
        </div>
        <?php if ($this->session->flashdata('error_message')) { ?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error_message'); ?>
            </div>
        <?php } ?>s
        <?php if ($this->session->flashdata('success_message')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success_message'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Admin list</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email Address</th>
                                <th>Contact Number</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($users as $user) { ?>
                                <tr>
                                    <td><?php echo $user['first_name']; ?></td>
                                    <td><?php echo $user['last_name']; ?></td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td><?php echo $user['phone']; ?></td>
                                    <td><a href="<?php echo base_url(); ?>user/edit/<?php echo $user['id']; ?>" >Edit</a></td>
                                    <td><a href="<?php echo base_url(); ?>user/delete/<?php echo $user['id']; ?>" >X</a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
<div class="row">
    <div class="col-lg-12">
        <h1>Move Clients to new Stylist</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <section class="container">
            <div class="container-page">	
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-rss"></i> Move Clients</h3>
                    </div>
                    <div class="panel-body feed" id="move_clients">
                        <?php if ($this->session->flashdata('success_message')) { ?>
                            <div class="alert alert-success">
                                <?php echo $this->session->flashdata('success_message'); ?>
                            </div>
                        <?php } ?>
                        <form role="form" action="<?php echo base_url(); ?>user/move_clients" method="post" >    
                            <div class="col-md-6">
                                <div class="form-group col-lg-12">
                                    <label>Client</label>
                                    <select name="client" class="form-control" id="client">
                                        <option value="all">All</option>
                                        <?php foreach ($clients as $client) { ?>
                                            <option value="<?php echo $client['id']; ?>"><?php echo $client['first_name'] . ' ' . $client['last_name']; ?></option>
                                        <?php } ?>
                                    </select>    
                                </div>
                                <div class="form-group col-lg-12" id="from_stylist_cont" >
                                    <label >From Stylist</label>
                                    <select name="from_stylist" class="form-control" >
                                        <?php foreach ($stylists as $stylist) { ?>
                                            <option value="<?php echo $stylist['id']; ?>"><?php echo $stylist['first_name'] . ' ' . $stylist['last_name']; ?></option>
                                        <?php } ?>
                                    </select>    
                                </div>
                                <div class="form-group col-lg-12">
                                    <label >To Stylist</label>
                                    <select name="to_stylist" class="form-control" >
                                        <?php foreach ($stylists as $stylist) { ?>
                                            <option value="<?php echo $stylist['id']; ?>"><?php echo $stylist['first_name'] . ' ' . $stylist['last_name']; ?></option>
                                        <?php } ?>
                                    </select>    
                                </div>
                                <div class="form-group col-lg-12"><br>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Move Clients</button>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-6">

                                <h3 class="dark-grey">Easy as 1 - 2 - 3</h3>
                                <p>
                                    Information can be displayed here on how to work the system
                                </p>
                                <p>
                                    Information can be displayed here on how to work the system Information can be displayed here on how to work the system- 
                                    Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                                </p>
                                <p>
                                    Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                                </p>
                                <p>
                                    Information can be displayed here on how to work the system Information can be displayed here on how to work the system
                                </p>
                            </div>
                        </form>
                    </div>
                    </section>
                </div>
            </div>
    </div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
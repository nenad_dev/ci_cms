<div class="panel-body" id="appointments" >                                                        
    <div class="col-md-4">
        <table class="table">
            <thead>
                <tr>
                    <th>Time</th>
                    <th>Avaliability</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($appointments_selected as $appointment_selected_key => $appointment_selected_value) { ?>
                <tr>
                    <td class="time"><?php echo $appointment_selected_key; ?></td>
                    <?php if ($appointment_selected_value == "n") { ?>
                    <td class="unavaliable">Unavaliable</td>
                    <?php } else { ?>
                    <td class="avaliable"><span class="date" style="display:none;" ><?php echo $date_selected;?></span><a href="">Avaliable</a></td>
                    <?php } ?>     
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        <table class="table" >
            <thead>
                <tr>
                    <th>Time</th>
                    <th><?php echo $date_1day_after;?></th>
                    <th><?php echo $date_2days_after;?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($appointments_1day_after as $appointment_1day_after_key => $appointment_1day_after_value) { ?>
                <tr>
                    <td class="time" ><?php echo $appointment_1day_after_key; ?></td>
                    <?php if ($appointments_1day_after[$appointment_1day_after_key] == "n") { ?>
                    <td class="unavaliable">Unavaliable</td>
                    <?php } else { ?>
                    <td class="avaliable"><span class="date" style="display:none;" ><?php echo $date_1day_after;?></span><a href="">Avaliable</a></td>
                    <?php } ?> 
                    <?php if ($appointments_2days_after[$appointment_1day_after_key] == "n") { ?>
                    <td class="unavaliable">Unavaliable</td>
                    <?php } else { ?>
                    <td class="avaliable"><span class="date" style="display:none;" ><?php echo $date_2days_after;?></span><a href="">Avaliable</a></td>
                    <?php } ?> 
                </tr>
                <?php } ?>                            
            </tbody>
        </table>
    </div>
</div>
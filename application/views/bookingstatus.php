<div class="row">
    <div class="col-lg-12">
        <h1>Booking Status</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Please make sure that the details below are correct and up to date before adding the service. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Booking Details</h3></div>
            <div class="panel-body feed">
                <?php if ($this->session->flashdata('error_message')) { ?>
                    <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_message'); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('success_message')) { ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_message'); ?>
                    </div>
                <?php } ?>
                <section class="feed-item">
                    <form role="form" action="<?php echo base_url(); ?>appointment/booking_status/<?php echo $this->uri->segment(3); ?>" method="post" >
                        <div class="col-md-6">      
                            <div class="form-group col-lg-12">
                                <label>Full Name</label>
                                <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['client_full_name']; ?>" type="text"></fieldset>
                            </div>

                            <div class="form-group col-lg-12">
                                <label>Email Address</label>
                                <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['client_email']; ?>" type="text"></fieldset>
                            </div>

                            <div class="form-group col-lg-12">
                                <label>Contact Number</label>
                                <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['client_phone']; ?>" type="text"></fieldset>
                            </div>
                        </div>

                        <div class="col-md-6">      
                            <div class="form-group col-lg-12">
                                <label>Stylist</label>
                                <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['stylist_full_name']; ?>" type="text"></fieldset>
                            </div>

                            <div class="form-group col-lg-12">
                                <label>Service Chosen</label>
                                <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['service_name']; ?>" type="text"></fieldset>
                            </div>

                            <div class="form-group col-lg-12">
                                <label>Date</label>
                                <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['appointment_start_date']; ?>" type="text"></fieldset>
                            </div>

                            <div class="form-group col-lg-12">
                                <label>Time</label>
                                <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['appointment_start_hour_min']; ?>" type="text"></fieldset>
                            </div>
                        </div>

                        <div class="col-md-6">  
                            <input type="submit" name="submit" class="btn btn-primary btn-lg" value="Approve" />
                            <input type="submit" name="submit" class="btn btn-primary btn-lg" value="Deny" />
                        </div>
                    </form>
                </section>

            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> History</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Appointment History</th>
                                <th>Stylist</th>
                                <th>Service</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>15 Dec 2015</td>
                                <td>Stylist Name</td>
                                <td>Cut</td>
                            </tr>
                            <tr>
                                <td>18 Dec 2015</td>
                                <td>Stylist Name</td>
                                <td>Upstyle</td>
                            </tr>
                            <tr>
                                <td>19 Dec 2015</td>
                                <td>Stylist Name</td>
                                <td>Blow Wave</td>
                            </tr></tbody>
                    </table>

                </section>
            </div>
        </div>
    </div>

</div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
<?php

class Head_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function set_head_scripts() {
        $script = '<script src="' . base_url() . 'assets/js/jquery-1.10.2.min.js" type="text/javascript"></script>' . "\n";
        $script .= '<script src="' . base_url() . 'assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>' . "\n";
        $script .= '<script src="' . base_url() . 'assets/js/js-functions.js" type="text/javascript"></script>' . "\n";
        $script .= '<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>'. "\n";

        $script .= '<script type="text/javascript">';
        $script .= 'var base_url = "' . base_url() . '";';
        $script .= '</script>'. "\n";

        return $script;
    }

    public function set_head_css() {
        $css = '<link href = "' . base_url() . 'assets/bootstrap/css/bootstrap.min.css" rel = "stylesheet" type = "text/css" />' . " \n";
        $css .= '<link href = "' . base_url() . 'assets/font-awesome/css/font-awesome.min.css" rel = "stylesheet" type = "text/css" />' . " \n";
        $css .= '<link href = "' . base_url() . 'assets/css/local.css" rel = "stylesheet" type = "text/css" />' . " \n";
        $css .= '<link rel = "stylesheet" href = "//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

        ';

        return $css;
    }

    public function set_head_page_title() {
        return "Dashboard";
    }

}
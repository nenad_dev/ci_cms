<div class="row">
    <div class="col-lg-12">
        <h1>Owner Dashboard</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your Dashboard, this gives you an overview of who is currently within the system. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Latest Admins</h3>
            </div>
            <div class="panel-body feed">
                <?php foreach ($admins as $admin) { ?>
                    <section class="feed-item">
                        <div class="icon pull-left">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="feed-item-body">
                            <div class="text">
                                <a href="<?php echo base_url(); ?>user/profile/<?php echo $admin['id']; ?>"><?php echo $admin['first_name'] . ' ' . $admin['last_name']; ?></a>
                            </div>
                            <div class="time pull-left">
                                Admin Since:  <?php echo date('l, d F Y', strtotime($admin['date_created'])); ?> at 
                                <?php echo date('h:ia', strtotime($admin['date_created'])); ?>
                            </div>
                        </div>
                    </section>
                <?php } ?>
            </div>
        </div>
        <a href="<?php echo base_url(); ?>dashboard/viewadmins" class="btn btn-primary btn-lg">View all</a>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Latest Stylists</h3>
            </div>

            <div class="panel-body feed">
                <?php foreach ($stylists as $stylist) { ?>
                    <section class="feed-item">
                        <div class="icon pull-left">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="feed-item-body">
                            <div class="text">
                                <a href="<?php echo base_url(); ?>user/profile/<?php echo $stylist['id']; ?>"><?php echo $stylist['first_name'] . ' ' . $stylist['last_name']; ?></a>
                            </div>
                            <div class="time pull-left">
                                Stylist Since: <?php echo date('l, d F Y', strtotime($stylist['date_created'])); ?> at 
                                <?php echo date('h:ia', strtotime($stylist['date_created'])); ?> 
                            </div>
                        </div>
                    </section>
                <?php } ?>
            </div>
        </div>
        <a href="<?php echo base_url(); ?>dashboard/viewstylists" class="btn btn-primary btn-lg">View all</a>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Latest Clients</h3>
            </div>
            <div class="panel-body feed">
                <?php foreach ($clients as $client) { ?>
                    <section class="feed-item">
                        <div class="icon pull-left">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="feed-item-body">
                            <div class="text">
                                <a href="<?php echo base_url(); ?>user/profile/<?php echo $client['id']; ?>"><?php echo $client['first_name'] . ' ' . $client['last_name']; ?></a>
                            </div>
                            <div class="time pull-left">
                                Client Since: <?php echo date('l, d F Y', strtotime($client['date_created'])); ?> at 
                                <?php echo date('h:ia', strtotime($client['date_created'])); ?> 
                            </div>
                        </div>
                    </section>
                <?php } ?>
            </div>
        </div>
        <a href="<?php echo base_url(); ?>dashboard/viewclients" class="btn btn-primary btn-lg">View all</a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Latest Appointment History</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Date of Appointment</th>
                                <th>Time of Appointment</th>
                                <th>Choice of Service</th>
                                <th>Client Name</th>
                                <th>Stylist</th>
                                <th>Service Rating</th>
                                <th>View more details</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>29 June 2015</td>
                                <td>14:00</td>
                                <td>Upstyle</td>
                                <td>Tom Gerry</td>
                                <td>Stylist Name</td>
                                <td>4.6/5.0</td>
                                <td>Click here</td>
                            </tr>
                            <tr>
                                <td>13 April 2015</td>
                                <td>10:30</td>
                                <td>Colour and Blow-wave</td>
                                <td>Tom Gerry</td>
                                <td>Stylist Name</td>
                                <td>4.8/5.0</td>
                                <td>Click here</td>
                            </tr>
                            <tr>
                                <td>10 February 2015</td>
                                <td>09:00</td>
                                <td>Upstyle</td>
                                <td>Tom Gerry</td>
                                <td>Stylist Name</td>
                                <td>4.2/5.0</td>
                                <td>Click here</td>
                            </tr>
                            <tr>
                                <td>20 December 2014</td>
                                <td>12:00</td>
                                <td>Colour</td>
                                <td>Tom Gerry</td>
                                <td>Stylist Name</td>
                                <td>4.8/5.0</td>
                                <td>Click here</td>
                            </tr>
                            <tr>
                                <td>15 September 2014</td>
                                <td>11:45</td>
                                <td>Upstyle</td>
                                <td>Tom Gerry</td>
                                <td>Stylist Name</td>
                                <td>4.9/5.0</td>
                                <td>Click here</td>
                            </tr>
                        </tbody>
                    </table>

                </section>
            </div>
        </div>
        <button type="button" class="btn btn-primary btn-lg">View all</button>
    </div> </div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
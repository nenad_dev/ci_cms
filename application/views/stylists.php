<div class="row">
    <div class="col-lg-12">
        <h1>Our Stylists</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your Stylists Page! Choose your stylist, view their portfolio and book your appointment today! 
            <br />
            The power is in your hands, build your profile of styles today!
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-3">
        <div class="well">
            <img class="thumbnail img-responsive" alt="Bootstrap template" src="<?php echo base_url() . 'admin_assets/images/h8.jpg';?>" />
            <span>
                <ul>
                    <li>Full Name</li>
                    <li>Quote from Stylist</li>
                    <li>View Portfolio</li>
                </ul>
            </span>
        </div>
    </div>

    <div class="col-md-3">
        <div class="well">
            <img class="thumbnail img-responsive" alt="Bootstrap template" src="<?php echo base_url() . 'admin_assets/images/h8.jpg';?>" />
            <span>
                <ul>
                    <li>Full Name</li>
                    <li>Quote from Stylist</li>
                    <li>View Portfolio</li>
                </ul>
            </span>
        </div>
    </div>

    <div class="col-md-3">
        <div class="well">
            <img class="thumbnail img-responsive" alt="Bootstrap template" src="<?php echo base_url() . 'admin_assets/images/h8.jpg';?>" />
            <span>
                <ul>
                    <li>Full Name</li>
                    <li>Quote from Stylist</li>
                    <li>View Portfolio</li>
                </ul>
            </span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="well">
            <img class="thumbnail img-responsive" alt="Bootstrap template" src="<?php echo base_url() . 'admin_assets/images/h8.jpg';?>" />
            <span>
                <ul>
                    <li>Full Name</li>
                    <li>Quote from Stylist</li>
                    <li>View Portfolio</li>
                </ul>
            </span>
        </div>
    </div>

    <div class="col-md-3">
        <div class="well">
            <img class="thumbnail img-responsive" alt="Bootstrap template" src="<?php echo base_url() . 'admin_assets/images/h8.jpg';?>" />
            <span>
                <ul>
                    <li>Full Name</li>
                    <li>Quote from Stylist</li>
                    <li>View Portfolio</li>
                </ul>
            </span>
        </div>
    </div>

    <div class="col-md-3">
        <div class="well">
            <img class="thumbnail img-responsive" alt="Bootstrap template" src="<?php echo base_url() . 'admin_assets/images/h8.jpg';?>" />
            <span>
                <ul>
                    <li>Full Name</li>
                    <li>Quote from Stylist</li>
                    <li>View Portfolio</li>
                </ul>
            </span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="well">
            <img class="thumbnail img-responsive" alt="Bootstrap template" src="<?php echo base_url() . 'admin_assets/images/h8.jpg';?>" />
            <span>
                <ul>
                    <li>Full Name</li>
                    <li>Quote from Stylist</li>
                    <li>View Portfolio</li>
                </ul>
            </span>
        </div>
    </div>

    <div class="col-md-3">
        <div class="well">
            <img class="thumbnail img-responsive" alt="Bootstrap template" src="<?php echo base_url() . 'admin_assets/images/h8.jpg';?>" />
            <span>
                <ul>
                    <li>Full Name</li>
                    <li>Quote from Stylist</li>
                    <li>View Portfolio</li>
                </ul>
            </span>
        </div>
    </div>

</div>



</div>
</div>
<!-- /#wrapper -->

<script type="text/javascript">
    jQuery(function ($) {
        var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
        visits = [123, 323, 443, 32],
        traffic = [
            {
                Source: "Direct", Amount: 323, Change: 53, Percent: 23, Target: 600
            },
            {
                Source: "Refer", Amount: 345, Change: 34, Percent: 45, Target: 567
            },
            {
                Source: "Social", Amount: 567, Change: 67, Percent: 23, Target: 456
            },
            {
                Source: "Search", Amount: 234, Change: 23, Percent: 56, Target: 890
            },
            {
                Source: "Internal", Amount: 111, Change: 78, Percent: 12, Target: 345
            }];


        $("#shieldui-chart1").shieldChart({
            theme: "dark",

            primaryHeader: {
                text: "Visitors"
            },
            exportOptions: {
                image: false,
                print: false
            },
            dataSeries: [{
                    seriesType: "area",
                    collectionAlias: "Q Data",
                    data: performance
                }]
        });

        $("#shieldui-chart2").shieldChart({
            theme: "dark",
            primaryHeader: {
                text: "Traffic Per week"
            },
            exportOptions: {
                image: false,
                print: false
            },
            dataSeries: [{
                    seriesType: "pie",
                    collectionAlias: "traffic",
                    data: visits
                }]
        });

        $("#shieldui-grid1").shieldGrid({
            dataSource: {
                data: traffic
            },
            sorting: {
                multiple: true
            },
            rowHover: false,
            paging: false,
            columns: [
                { field: "Source", width: "170px", title: "Source" },
                { field: "Amount", title: "Amount" },                
                { field: "Percent", title: "Percent", format: "{0} %" },
                { field: "Target", title: "Target" },
            ]
        });            
    });        
</script>
</body>
</html>
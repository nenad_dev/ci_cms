<div class="row">
    <div class="col-lg-12">
        <h1>List of Clients</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your list of Clients Page. 
        </div>
        <?php if ($this->session->flashdata('error_message')) { ?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error_message'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success_message')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success_message'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Clients list</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email Address</th>
                                <th>Contact Number</th>
                                <th>Stylist</th>
                                <th>Last Visit</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($users as $user) { ?>
                                <tr>
                                    <td><a href="<?php echo base_url(); ?>user/profile/<?php echo $user['id']; ?>" ><?php echo $user['first_name']; ?></a</td>
                                    <td><a href="<?php echo base_url(); ?>user/profile/<?php echo $user['id']; ?>" ><?php echo $user['last_name']; ?></a</td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td><?php echo $user['phone']; ?></td>
                                    <td><a href="<?php echo base_url(); ?>user/profile/<?php echo $user['stylist_id']; ?>" ><?php echo $user['stylist_full_name']; ?></a></td>
                                    <td>Date</td>
                                    <?php if (($role == 1) || ($role == 2)) { ?>
                                        <td><a href="<?php echo base_url(); ?>user/edit/<?php echo $user['id']; ?>" >Edit</a></td>
                                        <td><a href="<?php echo base_url(); ?>user/delete/<?php echo $user['id']; ?>" >X</a></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
<?php

class Dashboard_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_appointments() {
        $this->db->select('*');
        $this->db->from('appointment');
        $result = $this->db->get();

        return $result->result_array();
    }

}
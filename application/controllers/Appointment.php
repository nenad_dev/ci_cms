<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Appointment extends CI_Controller {

    public function __construct() {
        parent::__construct();

// if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        }
    }

    function add_category() {
// load models
        $this->load->model('appointment_model');

// validate add category form fields
        $this->form_validation->set_rules('category_name', 'Category Name', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('parent_id', 'Parent id', 'trim|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $category_name = $this->input->post('category_name');
            $parent_id = $this->input->post('parent_id');

            $response = $this->appointment_model->set_add_category($category_name, $parent_id);

            if ($response) {
                $this->session->set_flashdata('success_message', 'Category successfully added!');
            } else {
                $this->session->set_flashdata('error_message', 'Some error appeared!');
            }

            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function edit_category() {
// load models
        $this->load->model('appointment_model');

// validate add category form fields
        $this->form_validation->set_rules('category_name', 'Category Name', 'trim|min_length[2]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $category_name = $this->input->post('category_name');

            $id = $this->uri->segment(3);
            $response = $this->appointment_model->set_edit_category($category_name, $id);

            if ($response) {
                $this->session->set_flashdata('success_message', 'Category successfully edited!');
            } else {
                $this->session->set_flashdata('error_message', 'Some error appeared!');
            }
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function add_service() {
        $this->_add_edit_service('add');
    }

    public function edit_service() {
        $this->_add_edit_service('edit');
    }

    public function _add_edit_service($action) {

// load models
        $this->load->model('appointment_model');

// validate add category form fields
        $this->form_validation->set_rules('category', 'Category', 'required|trim|xss_clean');
        $this->form_validation->set_rules('subcategory', 'Subcategory', 'required|trim|xss_clean');
        $this->form_validation->set_rules('service_name', 'Service Name', 'required|trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('duration_in_hours', 'Duration In Chair Hours', 'required|trim|max_length[2]|xss_clean');
        $this->form_validation->set_rules('duration_in_mins', 'Duration Out Chair Mins', 'required|trim|max_length[2]|xss_clean');
        $this->form_validation->set_rules('duration_out_hours', 'Duration Out Chair Hours', 'required|trim|max_length[2]|xss_clean');
        $this->form_validation->set_rules('duration_out_mins', 'Duration Out Chair Mins', 'required|trim|max_length[2]|xss_clean');
        $this->form_validation->set_rules('cost', 'Cost', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $category = $this->input->post('category');
            $subcategory = $this->input->post('subcategory');
            $gender = $this->input->post('gender');
            $service_name = $this->input->post('service_name');
            $cost = $this->input->post('cost');
            $duration_in_hours = $this->input->post('duration_in_hours');
            $duration_in_mins = $this->input->post('duration_in_mins');
            $duration_out_hours = $this->input->post('duration_out_hours');
            $duration_out_mins = $this->input->post('duration_out_mins');

            if ($action == 'add') {
                $response = $this->appointment_model->set_add_service($category, $subcategory, $gender, $service_name, $cost, $duration_in_hours, $duration_in_mins, $duration_out_hours, $duration_out_mins);
            } else {
                $id = $this->uri->segment(3);
                $response = $this->appointment_model->set_edit_service($category, $subcategory, $gender, $service_name, $cost, $duration_in_hours, $duration_in_mins, $duration_out_hours, $duration_out_mins, $id);
            }

            if ($response) {
                if ($action == 'add') {
                    $this->session->set_flashdata('success_message', 'Service successfully added!');
                } else {
                    $this->session->set_flashdata('success_message', 'Service successfully edited!');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Some error appeared!');
            }
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function delete_category() {

// allow deleting users only for owner
        if (($this->session->userdata('user_role') != 1)) {
            redirect('/');
        }

// load models
        $this->load->model('appointment_model');

        $id = $this->uri->segment(3);
        $response = $this->appointment_model->set_delete_category($id);

        if ($response == 'parent_subcategory') {
            $this->session->set_flashdata('error_message', 'This category can not be deleted! It contains Subcategories!');
        } elseif ($response == 'cat_parent_service') {
            $this->session->set_flashdata('error_message', 'This category can not be deleted! It contains Services!');
        } elseif ($response == 'subcat_parent_service') {
            $this->session->set_flashdata('error_message', 'This subcategory can not be deleted! It contains Services!');
        } elseif ($response) {
            $this->session->set_flashdata('success_message', 'Category successfully deleted!');
        } else {
            $this->session->set_flashdata('error_message', 'Some error appeared!');
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function delete_service() {

// allow deleting users only for owner
        if (($this->session->userdata('user_role') != 1)) {
            redirect('/');
        }

// load models
        $this->load->model('appointment_model');

        $id = $this->uri->segment(3);
        $this->appointment_model->set_delete_service($id);
        $this->session->set_flashdata('success_message', 'Category successfully deleted!');

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function ajax_get_subcategories_for_category() {
        if ($this->input->post('ajax') == 1) {
            $id = $this->input->post('id');

            $this->load->model('appointment_model');
            $response['items'] = $this->appointment_model->get_subcategories_for_category($id);
            $response['status'] = 'ok';
        } else {
            $response['status'] = 'error';
        }

        echo json_encode($response);
    }

    public function ajax_get_services_for_subcategory() {
        if ($this->input->post('ajax') == 1) {
            $category_id = $this->input->post('category_id');
            $subcategory_id = $this->input->post('subcategory_id');

            $this->load->model('appointment_model');
            $response['items'] = $this->appointment_model->get_services_for_category($category_id, $subcategory_id);
            $response['status'] = 'ok';
        } else {
            $response['status'] = 'error';
        }

        echo json_encode($response);
    }

    public function ajax_get_stylist_calendar() {

        $this->load->model('appointment_model');
        $user_id = $this->session->userdata('user_id');

        $date = $this->input->post('date');

        $stylist_calendar['stylist_calendar_items'] = $this->appointment_model->get_stylist_calendar_items($user_id, $date);

        echo $this->load->view('stylist_calendar', $stylist_calendar, TRUE);
    }

    public function ajax_set_appointment() {
        if ($this->input->post('ajax') == 1) {
            $date = $this->input->post('date');
            $service_id = $this->input->post('service');
            $this->load->model('appointment_model');
            $user_id = $this->session->userdata('user_id');
            $appointment_id = $this->appointment_model->set_appointment($user_id, $service_id, $date);
            if ($appointment_id)
                $response = 'ok';
            else
                $response = 'error';
        } else {
            $response = 'error';
        }

        echo $response;
    }

    public function booking_status() {

// allow deleting users only for owner
        $role = $this->session->userdata('user_role');
        if (($role != 1) && ($role != 2)) {
            redirect('/');
        }

// load models
        $this->load->model('appointment_model');

        $submit = $this->input->post('submit');
        if ($submit == 'Approve') {
            $status = 1;
        } elseif ($submit == 'Deny') {
            $status = 2;
        } else {
            $status = 0;
        }

        $id = $this->uri->segment(3);
        $response = $this->appointment_model->set_change_booking_status($id, $status);
        if ($response) {
            $this->session->set_flashdata('success_message', 'Status successfully changed!');
        } else {
            $this->session->set_flashdata('error_message', 'Some error appeared!');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

}

<?php

/* ------  Booking Status --------- */
/*
  /*    0 - New/Unresolved
  /*    1 - Approved
  /*    2 - Denied
  /*    3 - Cancelled
  /*
  /*----------------------------- */

class Appointment_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function set_add_category($category_name, $parent_id) {

        $data = array(
            'name' => $category_name,
            'parent_id' => $parent_id
        );

        $this->db->insert('category', $data);
        $category_id = $this->db->insert_id();
        return $category_id;
    }

    function set_edit_category($category_name, $id) {

        $data = array(
            'name' => $category_name
        );

        $this->db->where('id', $id);
        return $this->db->update('category', $data);
    }

    function set_delete_category($id) {

        // check does category contain subcategories
        $this->db->where('parent_id', $id);
        $result1 = $this->db->get('category');
        if ($result1->num_rows() > 0) {
            return "parent_subcategory";
        }

        // check does category contain services
        $this->db->where('category_id', $id);
        $result2 = $this->db->get('service');
        if ($result2->num_rows() > 0) {
            return "cat_parent_service";
        }

        // check does category contain services
        $this->db->where('subcategory_id', $id);
        $result3 = $this->db->get('service');
        if ($result3->num_rows() > 0) {
            return "subcat_parent_service";
        }

        $this->db->where('id', $id);
        $this->db->delete('category');
        return $this->db->affected_rows();
    }

    function get_categories($cat_type) {
        $this->db->select('*');
        $this->db->from('category');
        if ($cat_type == "main_cat") {
            $this->db->where('parent_id', 0);
        } elseif ($cat_type == "sub_cat") {
            $this->db->where('parent_id <>', 0);
        }
        $result = $this->db->get();
        $result_array = $result->result_array();

        if ($cat_type == "sub_cat") {
            foreach ($result_array as $key => $value) {
                $result_array[$key]["main_cat_name"] = $this->get_category_name($value["parent_id"]);
            }
        }

        return $result_array;
    }

    function get_category_info($id) {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('id', $id);
        $result = $this->db->get();
        $result_array = $result->result_array();
        return $result_array[0];
    }

    function get_service_info($id) {
        $this->db->select('*');
        $this->db->from('service');
        $this->db->where('id', $id);
        $result = $this->db->get();
        $result_array = $result->result_array();
        $result_array_final = $result_array[0];

        $duration_in_hours = floor($result_array_final["duration_in"] / 60);
        $duration_in_mins = $result_array_final["duration_in"] - $duration_in_hours * 60;
        $duration_out_hours = floor($result_array_final["duration_out"] / 60);
        $duration_out_mins = $result_array_final["duration_out"] - $duration_out_hours * 60;

        $result_array_final['duration_in_hours'] = $duration_in_hours;
        $result_array_final['duration_in_mins'] = $duration_in_mins;
        $result_array_final['duration_out_hours'] = $duration_out_hours;
        $result_array_final['duration_out_mins'] = $duration_out_mins;

        return $result_array_final;
    }

    function get_service_in_chair_duration($id) {
        $this->db->select('duration_in');
        $this->db->from('service');
        $this->db->where('id', $id);
        $result = $this->db->get();
        $duration_in = $result->row(0)->duration_in;

        return $duration_in;
    }

    function get_category_name($id) {
        $this->db->select('name');
        $this->db->from('category');
        $this->db->where('id', $id);
        $result = $this->db->get();
        if (isset($result->row(0)->name)) {
            return $result->row(0)->name;
        } else {
            return 'None';
        }
    }

    function set_add_service($category, $subcategory, $gender, $service_name, $cost, $duration_in_hours, $duration_in_mins, $duration_out_hours, $duration_out_mins) {

        $duration_in = $duration_in_hours * 60 + $duration_in_mins;
        $duration_out = $duration_out_hours * 60 + $duration_out_mins;

        $data = array(
            'category_id' => $category,
            'subcategory_id' => $subcategory,
            'gender' => $gender,
            'name' => $service_name,
            'cost' => $cost,
            'duration_in' => $duration_in,
            'duration_out' => $duration_out
        );

        $this->db->insert('service', $data);
        $service_id = $this->db->insert_id();
        return $service_id;
    }

    function set_edit_service($category, $subcategory, $gender, $service_name, $cost, $duration_in_hours, $duration_in_mins, $duration_out_hours, $duration_out_mins, $id) {

        $duration_in = $duration_in_hours * 60 + $duration_in_mins;
        $duration_out = $duration_out_hours * 60 + $duration_out_mins;

        $data = array(
            'category_id' => $category,
            'subcategory_id' => $subcategory,
            'gender' => $gender,
            'name' => $service_name,
            'cost' => $cost,
            'duration_in' => $duration_in,
            'duration_out' => $duration_out
        );

        $this->db->where('id', $id);
        $this->db->update('service', $data);
        return $this->db->affected_rows();
    }

    function set_delete_service($id) {
        $this->db->where('id', $id);
        $this->db->delete('service');
    }

    function get_services() {
        $this->db->select('*');
        $this->db->from('service');
        $result = $this->db->get();
        $result_array = $result->result_array();
        foreach ($result_array as $key => $value) {
            $result_array[$key]["category"] = $this->get_category_name($value["category_id"]);
            $result_array[$key]["subcategory"] = $this->get_category_name($value["subcategory_id"]);

            $duration = $value["duration_in"] + $value["duration_out"];
            $duration_hours = floor($duration / 60);
            $duration_mins = $duration - $duration_hours * 60;

            $result_array[$key]["duration_hours"] = $duration_hours;
            $result_array[$key]["duration_mins"] = $duration_mins;
        }

        return $result_array;
    }

    function get_subcategories_for_category($id) {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('parent_id', $id);
        $result = $this->db->get();
        $result_array = $result->result_array();
        return $result_array;
    }

    function get_services_for_category($category_id, $subcategory_id) {
        $this->db->select('id, name');
        $this->db->from('service');
        $this->db->where('category_id', $category_id);
        $this->db->where('subcategory_id', $subcategory_id);
        $result = $this->db->get();
        $result_array = $result->result_array();
        return $result_array;
    }

    function get_stylist_work_hours($stylist_id, $day) {

        $this->db->select('*');
        $this->db->from('stylist_work_days');
        $this->db->where('stylist_id', $stylist_id);
        $this->db->where('day', $day);
        $result = $this->db->get();
        $result_array = $result->result_array();
        return $result_array[0];
    }

    function get_stylist_daily_availability($date, $stylist_id, $service_duration) {

        // initialize appointment terms array
        $appointment_terms = $this->get_appointment_terms();

        // get stylist work hours for given date
        $day = strtolower(date("D", strtotime($date)));
        $stylist_work_hours = $this->get_stylist_work_hours($stylist_id, $day);

        // initialize availability array
        $availability = array();
        foreach ($appointment_terms as $appointment_term) {
            $availability[$appointment_term] = "n";
        }

        // initialize appointment terms array
        if (($stylist_work_hours['start'] == '00:00') && ($stylist_work_hours['end'] == '00:00')) {
            return $availability;
        }

        // set availability for stylist work hours
        $swh_start_parts = explode(':', $stylist_work_hours['start']);
        $swh_start_hour = $swh_start_parts[0];
        $swh_start_min = $swh_start_parts[1];
        $swh_end_parts = explode(':', $stylist_work_hours['end']);
        $swh_end_hour = $swh_end_parts[0];
        $swh_end_min = $swh_end_parts[1];

        $swh_appointment_terms = $this->get_appointment_terms($swh_start_hour, $swh_start_min, $swh_end_hour, $swh_end_min);
        foreach ($swh_appointment_terms as $swh_appointment_term) {
            if (in_array($swh_appointment_term, $appointment_terms)) {
                $availability[$swh_appointment_term] = "y";
            }
        }

        // get appointments for stylist with given $stylist_id for given $date
        $query = "SELECT DATE_FORMAT(`appointment`.`date`,'%H') AS `appointment_start_hour`,DATE_FORMAT(`appointment`.`date`,'%i') AS `appointment_start_min`,`service`.`duration_in`
		FROM `appointment`
	    LEFT JOIN `client_stylist` ON `appointment`.`client_id` = `client_stylist`.`client_id`";
        $query.= " LEFT JOIN `service` ON `appointment`.`service_id` = `service`.`id`";
        $query.= " WHERE `client_stylist`.`stylist_id` = " . $stylist_id;
        $query.= " AND  DATE(`appointment`.`date`) = '" . $date . "'";

        $result = $this->db->query($query);
        $items = $result->result_array();

        // set not available terms
        foreach ($items as $item) {
            $start_hour = $item['appointment_start_hour'];
            $start_min = $item['appointment_start_min'];
            $end_hour = $start_hour + floor($item['duration_in'] / 60);
            $end_min = date("i", strtotime('+' . $item['duration_in'] . ' minutes', strtotime($start_hour . ':' . $start_min)));
            $item_appointment_terms = $this->get_appointment_terms($start_hour, $start_min, $end_hour, $end_min);
            foreach ($item_appointment_terms as $item_appointment_term) {
                $availability[$item_appointment_term] = "n";
            }
        }

        // check does some term can be start time - this is related with in chair duration
        $position = floor($service_duration / 15);
        foreach ($availability as $key => $value) {
            if ($availability[$key] == 'y') {
                $availability[$key] = $this->get_availability_in_range_of_nth($availability, $key, $position);
            }
        }

        return $availability;
    }

    function get_appointment_terms($start_hour = 8, $start_min = 0, $end_hour = 11, $end_min = 45) {

        // initialize appointment terms array
        $appointment_terms = array();

        // set minute intervals
        $mins = array(0, 15, 30, 45);

        // remove leading zero
        $start_hour = ltrim($start_hour, '0');
        $start_min = ltrim($start_min, '0');
        $end_hour = ltrim($end_hour, '0');
        $end_min = ltrim($end_min, '0');

        // create appointment terms array
        for ($i = $start_hour; $i <= $end_hour; $i++)
            foreach ($mins as $v) {

                if ((($i > $start_hour) && ($i < $end_hour)) || (($i == $start_hour) && ($v >= $start_min)) || (($i == $end_hour) && ($v <= $end_min))) {
                    $appointment_terms[] = sprintf("%02d", $i) . ":" . sprintf("%02d", $v);
                }
            }

        return $appointment_terms;
    }

    function get_availability_in_range_of_nth($items, $current_key, $n) {

        $start_count = FALSE;
        $counter = 0;
        foreach ($items as $key => $value) {
            if ($current_key == $key) {
                $start_count = TRUE;
            }

            if ($start_count) {
                if ($value == 'n') {
                    return 'n';
                }
                $counter++;
            }

            if ($counter == $n) {
                return $value;
            }
        }

        return 'n';
    }

    function set_appointment($client_id, $service_id, $date) {
        $data = array(
            'date' => $date,
            'client_id' => $client_id,
            'service_id' => $service_id,
            'status' => 0
        );

        $this->db->insert('appointment', $data);
        $appointment_id = $this->db->insert_id();
        return $appointment_id;
    }

    function get_booking_info($id, $status, $date_filter, $client_id) {

        $this->load->model('user_model');

        $query = "SELECT DATE_FORMAT(`appointment`.`date`,'%d %b %Y') AS `appointment_start_date`,DATE_FORMAT(`appointment`.`date`,'%H:%i') AS `appointment_start_hour_min`,`service`.`name` AS `service_name`,`user`.`first_name` AS `client_first_name`,`user`.`last_name` AS `client_last_name`,`appointment`.`id`,`client_stylist`.`stylist_id`";
        if ($id)
            $query.= ",`user`.`phone` AS `client_phone`,`user`.`email` AS `client_email`";
        $query.= " FROM `appointment`";
        $query.= " LEFT JOIN `client_stylist` ON `appointment`.`client_id` = `client_stylist`.`client_id`";
        $query.= " LEFT JOIN `user` ON `appointment`.`client_id` = `user`.`id`";
        $query.= " LEFT JOIN `service` ON `appointment`.`service_id` = `service`.`id`";
        if ($id)
            $query.= " WHERE `appointment`.`id` = '" . $id . "'";
        else
            $query.= " WHERE `appointment`.`status` = '" . $status . "'";

        if ($date_filter == 'history')
            $query.= " AND `appointment`.`date` < NOW()";
        elseif ($date_filter == 'active')
            $query.= " AND `appointment`.`date` >= NOW()";

        if ($client_id)
            $query.= " AND `appointment`.`client_id` = '" . $client_id . "'";

        $result = $this->db->query($query);
        $bookings = $result->result_array();

        foreach ($bookings as $key => $value) {
            $user_stylist_info = $this->user_model->get_profile_info($value['stylist_id']);
            $bookings[$key]['client_full_name'] = $value['client_first_name'] . ' ' . $value['client_last_name'];
            $bookings[$key]['stylist_full_name'] = $user_stylist_info['first_name'] . ' ' . $user_stylist_info['last_name'];
        }

        return $bookings;
    }

    function set_change_booking_status($id, $status) {
        $data = array(
            'status' => $status
        );

        $this->db->where('id', $id);
        $this->db->update('appointment', $data);
        return $this->db->affected_rows();
    }

    function get_stylist_calendar_item($stylist_id, $date) {

        // initialize appointment terms array
        $appointment_terms = $this->get_appointment_terms();

        // initialize calendar
        $calendar_items = array();
        foreach ($appointment_terms as $appointment_term) {
            $calendar_items[$appointment_term] = "-";
        }

        // get appointments for stylist with given $stylist_id for given $date
        $query = "SELECT DATE_FORMAT(`appointment`.`date`,'%H:%i') AS `appointment_start_hour_min`,`service`.`duration_in`,`appointment`.`client_id`,`user`.`first_name`
		FROM `appointment`
	    LEFT JOIN `client_stylist` ON `appointment`.`client_id` = `client_stylist`.`client_id`";
        $query.= " LEFT JOIN `service` ON `appointment`.`service_id` = `service`.`id`";
        $query.= " LEFT JOIN `user` ON `appointment`.`client_id` = `user`.`id`";
        $query.= " WHERE `client_stylist`.`stylist_id` = " . $stylist_id;
        $query.= " AND DATE(`appointment`.`date`) = '" . $date . "'";
        $query.= " AND `appointment`.`status` = '1'";

        $result = $this->db->query($query);
        $items = $result->result_array();

        foreach ($items as $item) {
            $total_appereance = floor($item["duration_in"] / 15);

            $start_count = FALSE;
            $counter = 0;

            foreach ($calendar_items as $calendar_key => $calendar_value) {
                if ($calendar_key == $item["appointment_start_hour_min"]) {
                    $start_count = TRUE;
                }

                if ($start_count) {
                    $calendar_items[$calendar_key] = $item["first_name"];
                    $counter++;
                }

                if ($counter == $total_appereance) {
                    break;
                }
            }
        }

        return $calendar_items;
    }

    function get_stylist_calendar_items($stylist_id, $date) {
        
        $stylist_calendar_items = array();
        
        // End date
        $end_date = date('Y-m-d', strtotime('+6 day', strtotime($date)));

        while (strtotime($date) <= strtotime($end_date)) {
            $stylist_calendar_items[$date] = $this->get_stylist_calendar_item($stylist_id, $date);
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }
        
        return $stylist_calendar_items;
    }

}

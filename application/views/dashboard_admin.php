<div class="row">
    <div class="col-lg-12">
        <h1>Admin/Reception Dashboard</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your Dashboard, here you can view all Stylists Appointments an New Booking Requests. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Profile Information</h3>
            </div>
            <div class="panel-body feed">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Time of Appointment</th>
                            <th>Choice of Service</th>
                            <th>Stylist</th>
                            <th>View more details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>14:00</td>
                            <td>Upstyle</td>
                            <td>Stylist Name</td>
                            <td>Click here</td>
                        </tr>
                        <tr>
                            <td>10:30</td>
                            <td>Colour and Blow-wave</td>
                            <td>Stylist Name</td>
                            <td>Click here</td>
                        </tr>
                        <tr>
                            <td>09:00</td>
                            <td>Upstyle</td>
                            <td>Stylist Name</td>
                            <td>Click here</td>
                        </tr>
                        <tr>
                            <td>12:00</td>
                            <td>Colour</td>
                            <td>Stylist Name</td>
                            <td>Click here</td>
                        </tr>
                        <tr>
                            <td>11:45</td>
                            <td>Upstyle</td>
                            <td>Stylist Name</td>
                            <td>Click here</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Booking Requests</h3>
            </div>
            <div class="panel-body feed">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th>Date</th>
                            <th>Client</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>14:00</td>
                            <td>June 2015</td>
                            <td>Client Name</td>
                            <td>Click here</td>
                        </tr>
                        <tr>
                            <td>10:30</td>
                            <td>June 2015</td>
                            <td>Client Name</td>
                            <td>Click here</td>
                        </tr>
                        <tr>
                            <td>09:00</td>
                            <td>June 2015</td>
                            <td>Client Name</td>
                            <td>Click here</td>
                        </tr>
                        <tr>
                            <td>12:00</td>
                            <td>June 2015</td>
                            <td>Client Name</td>
                            <td>Click here</td>
                        </tr>
                        <tr>
                            <td>11:45</td>
                            <td>June 2015</td>
                            <td>Stylist Name</td>
                            <td>Click here</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Appointment History</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Date of Appointment</th>
                                <th>Time of Appointment</th>
                                <th>Choice of Service</th>
                                <th>You Rated us</th>
                                <th>View more details</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($appointments as $appointment) { ?>
                                <tr>
                                    <td><?php echo $appointment['date'];?></td>
                                    <td><?php echo $appointment['time'];?></td>
                                    <td><?php echo $appointment['service'];?></td>
                                    <td><?php echo $appointment['rating'];?>/5.0</td>
                                    <td>Click here</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div> </div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
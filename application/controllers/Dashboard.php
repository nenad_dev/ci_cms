<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        } else {
            // load models
            $this->load->model('head_model');
            $this->load->model('dashboard_model');
        }
    }

    public function index() {

        // set variables needed for views
        $head = $this->_set_head();

        $user_id = $this->session->userdata('user_id');

        // load views
        $this->load->view('header', $head);
        if ($head['role'] == 1) {
            $this->load->model('user_model');

            $dashboard['admins'] = $this->user_model->get_users(2, 5, 'id,first_name,last_name,date_created');
            $dashboard['stylists'] = $this->user_model->get_users(3, 5, 'id,first_name,last_name,date_created');
            $dashboard['clients'] = $this->user_model->get_users(4, 5, 'id,first_name,last_name,date_created');

            $this->load->view('dashboard_owner', $dashboard);
        } elseif ($head['role'] == 2) {
            $dashboard['appointments'] = $this->dashboard_model->get_appointments();
            $this->load->view('dashboard_admin', $dashboard);
        } elseif ($head['role'] == 3) {
            $this->load->model('appointment_model');

            // Start date
            $date = date('Y-m-d');

            $stylist_calendar['stylist_calendar_items'] = $this->appointment_model->get_stylist_calendar_items($user_id, $date);

            $dashboard['stylist_calendar'] = $this->load->view('stylist_calendar', $stylist_calendar, TRUE);
            $this->load->view('dashboard_stylist', $dashboard);
        } elseif ($head['role'] == 4) {
            $this->load->model('appointment_model');

            $dashboard['upcoming_items'] = $this->appointment_model->get_booking_info('', 1, 'active', $user_id);
            $dashboard['history_items'] = $this->appointment_model->get_booking_info('', 1, 'history', $user_id);

            $this->load->view('dashboard_client', $dashboard);
        }
    }

    public function viewadmins() {
        $this->_viewusers(2, 'viewadmins');
    }

    public function addadmin() {

        // set variables needed for views
        $head = $this->_set_head();

        // load views
        if ($head['role'] == 1) {
            $this->load->view('header', $head);
            $this->load->view('addadmin');
        } else {
            redirect('/');
        }
    }

    public function viewstylists() {
        $this->_viewusers(3, 'viewstylists');
    }

    public function addstylist() {

        // set variables needed for views
        $head = $this->_set_head();

        // load views
        if ($head['role'] == 1) {

            $swd_info = array(
                'mon_start_time' => '00:00',
                'tue_start_time' => '00:00',
                'wed_start_time' => '00:00',
                'thu_start_time' => '00:00',
                'fri_start_time' => '00:00',
                'sat_start_time' => '00:00',
                'sun_start_time' => '00:00',
                'mon_end_time' => '00:00',
                'tue_end_time' => '00:00',
                'wed_end_time' => '00:00',
                'thu_end_time' => '00:00',
                'fri_end_time' => '00:00',
                'sat_end_time' => '00:00',
                'sun_end_time' => '00:00'
            );

            $stylist_work_days['swd_info'] = $swd_info;
            $addstylist['stylist_work_days'] = $this->load->view('stylist_work_days', $stylist_work_days, TRUE);

            $this->load->view('header', $head);
            $this->load->view('addstylist', $addstylist);
        } else {
            redirect('/');
        }
    }

    public function viewclients() {
        $this->_viewusers(4, 'viewclients', TRUE);
    }

    public function addclient() {

        // set variables needed for views
        $head = $this->_set_head();

        $this->load->model('user_model');
        $addclient['stylists'] = $this->user_model->get_users(3, '');

        // load views
        if ($head['role'] != 4) {
            $this->load->view('header', $head);
            $this->load->view('addclient', $addclient);
        }
    }

    function _set_head() {
        // set variables needed for views
        $head['js_scripts'] = $this->head_model->set_head_scripts();
        $head['css'] = $this->head_model->set_head_css();
        $head['page_title'] = $this->head_model->set_head_page_title();
        $head['role'] = $this->session->userdata('user_role');

        return $head;
    }

    function _viewusers($role, $view_name, $stylists_info_flag = FALSE) {

        // dissallow access to some part of sites related with user role
        if (($this->session->userdata('user_role') != 1) && ($role == 2)) {
            redirect('/');
        }

        // load model
        $this->load->model('user_model');

        // set variables needed for views
        $head = $this->_set_head();

        if ($stylists_info_flag) {
            $users = $this->user_model->get_users($role, '');
            foreach ($users as $key => $value) {
                $user_stylist_id = $this->user_model->get_stylist_id($value['id']);
                $user_stylist_info = $this->user_model->get_profile_info($user_stylist_id);

                $users[$key]['stylist_id'] = $user_stylist_id;
                $users[$key]['stylist_full_name'] = $user_stylist_info['first_name'] . ' ' . $user_stylist_info['last_name'];
            }

            $view['users'] = $users;
        } else {
            $view['users'] = $this->user_model->get_users($role, '');
        }

        // load views
        $this->load->view('header', $head);
        $this->load->view($view_name, $view);
    }

    public function contact() {

        // set variables needed for views
        $head = $this->_set_head();

        // load views
        $this->load->view('header', $head);
        $this->load->view('contact');
    }

    public function addcategory() {

        // set variables needed for views
        $head = $this->_set_head();

        $this->load->model('appointment_model');
        $addcategory['categories'] = $this->appointment_model->get_categories("main_cat");

        // load views
        if ($head['role'] == 1) {
            $this->load->view('header', $head);
            $this->load->view('addcategory', $addcategory);
        } else {
            redirect('/');
        }
    }

    public function editcategory() {

        // allow edit category only for owner
        if (($this->session->userdata('user_role') != 1)) {
            redirect('/');
        }

        $id = $this->uri->segment(3);

        // set variables needed for views
        $head = $this->_set_head();

        $this->load->model('appointment_model');
        $editcategory['category_info'] = $this->appointment_model->get_category_info($id);

        // load views
        if ($head['role'] == 1) {
            $this->load->view('header', $head);
            $this->load->view('editcategory', $editcategory);
        } else {
            redirect('/');
        }
    }

    public function viewcategories() {

        // set variables needed for views
        $head = $this->_set_head();

        $this->load->model('appointment_model');
        $viewcategories['categories'] = $this->appointment_model->get_categories("main_cat");
        $viewcategories['subcategories'] = $this->appointment_model->get_categories("sub_cat");

        // load views
        if ($head['role'] == 1) {
            $this->load->view('header', $head);
            $this->load->view('viewcategories', $viewcategories);
        } else {
            redirect('/');
        }
    }

    public function addservice() {

        // set variables needed for views
        $head = $this->_set_head();

        $this->load->model('appointment_model');
        $addservice['categories'] = $this->appointment_model->get_categories("main_cat");
        $addservice['subcategories'] = $this->appointment_model->get_categories("sub_cat");

        // load views
        if ($head['role'] == 1) {
            $this->load->view('header', $head);
            $this->load->view('addservice', $addservice);
        } else {
            redirect('/');
        }
    }

    public function editservice() {

        // set variables needed for views
        $head = $this->_set_head();

        $id = $this->uri->segment(3);

        $this->load->model('appointment_model');
        $editservice['categories'] = $this->appointment_model->get_categories("main_cat");
        $editservice['subcategories'] = $this->appointment_model->get_categories("sub_cat");
        $editservice['service_info'] = $this->appointment_model->get_service_info($id);

        // load views
        if ($head['role'] == 1) {
            $this->load->view('header', $head);
            $this->load->view('editservice', $editservice);
        } else {
            redirect('/');
        }
    }

    public function viewservices() {

        // set variables needed for views
        $head = $this->_set_head();

        if ($head['role'] == 1) {
            $this->load->model('appointment_model');
            $viewservices['services'] = $this->appointment_model->get_services();

            // load views
            $this->load->view('header', $head);
            $this->load->view('viewservices', $viewservices);
        } else {
            redirect('/');
        }
    }

    public function viewbookings() {

        // set variables needed for views
        $head = $this->_set_head();

        if (($head['role'] == 1) || ($head['role'] == 2)) {
            $this->load->model('appointment_model');

            $bookings = $this->appointment_model->get_booking_info('', 0, '', '');
            $viewbookings['bookings'] = $bookings;
            $viewbookings['own_bookings'] = FALSE;

            // load views
            $this->load->view('header', $head);
            $this->load->view('viewbookings', $viewbookings);
        } else {
            redirect('/');
        }
    }

    public function appointmenthistory() {

        // set variables needed for views
        $head = $this->_set_head();

        $this->load->model('appointment_model');

        $user_id = $this->session->userdata('user_id');
        $bookings = $this->appointment_model->get_booking_info('', 1, 'history', $user_id);
        $viewbookings['bookings'] = $bookings;
        $viewbookings['own_bookings'] = TRUE;

        // load views
        $this->load->view('header', $head);
        $this->load->view('viewbookings', $viewbookings);
    }

    public function bookingstatus() {

        // set variables needed for views
        $head = $this->_set_head();

        if (($head['role'] == 1) || ($head['role'] == 2)) {
            $this->load->model('appointment_model');

            $id = $this->uri->segment(3);
            $booking = $this->appointment_model->get_booking_info($id, '', '', '');
            $bookingstatus['booking'] = $booking[0];

            // load views
            $this->load->view('header', $head);
            $this->load->view('bookingstatus', $bookingstatus);
        } else {
            redirect('/');
        }
    }

    public function appointment() {

        // set variables needed for views
        $head = $this->_set_head();

        $this->load->model('appointment_model');

        $id = $this->uri->segment(3);
        $booking = $this->appointment_model->get_booking_info($id, '', '', '');
        $appointment['booking'] = $booking[0];

        // load views
        $this->load->view('header', $head);
        $this->load->view('appointment', $appointment);
    }

    public function bookappointment() {

        // set variables needed for views
        $head = $this->_set_head();

        $this->load->model('appointment_model');
        $this->load->model('user_model');
        $bookappointment['categories'] = $this->appointment_model->get_categories("main_cat");
        $user_id = $this->session->userdata('user_id');
        $bookappointment['client'] = $this->user_model->get_client_info($user_id);

        // load views
        $this->load->view('header', $head);
        $this->load->view('bookappointment', $bookappointment);
    }

    public function moveclients() {

        // set variables needed for views
        $head = $this->_set_head();

        if ($head['role'] == 1) {

            $this->load->model('user_model');
            $moveclients['clients'] = $this->user_model->get_users(4, '');
            $moveclients['stylists'] = $this->user_model->get_users(3, '');

            // load views
            $this->load->view('header', $head);
            $this->load->view('moveclients', $moveclients);
        }
    }

    public function ajax_get_appointment_availability_content() {

        $this->load->model('user_model');
        $this->load->model('appointment_model');
        $user_id = $this->session->userdata('user_id');
        $stylist_id = $this->user_model->get_stylist_id($user_id);

        $service_id = $this->input->post('service');
        $date_selected = $this->input->post('date');
        $date_1day_after = date('Y-m-d', strtotime($date_selected) + 86400);
        $date_2days_after = date('Y-m-d', strtotime($date_selected) + 86400 * 2);

        $service_in_duration = $this->appointment_model->get_service_in_chair_duration($service_id);

        $appointment_availability_content['appointments_selected'] = $this->appointment_model->get_stylist_daily_availability($date_selected, $stylist_id, $service_in_duration);
        $appointment_availability_content['appointments_1day_after'] = $this->appointment_model->get_stylist_daily_availability($date_1day_after, $stylist_id, $service_in_duration);
        $appointment_availability_content['appointments_2days_after'] = $this->appointment_model->get_stylist_daily_availability($date_2days_after, $stylist_id, $service_in_duration);

        $appointment_availability_content['date_selected'] = $date_selected;
        $appointment_availability_content['date_1day_after'] = $date_1day_after;
        $appointment_availability_content['date_2days_after'] = $date_2days_after;

        echo $this->load->view('appointment_availability_content', $appointment_availability_content, TRUE);
    }

}

<div id="step1">
    <div class="row">
        <div class="col-lg-12">
            <h1>Select Service for Appointment</h1>
            <div class="alert alert-dismissable alert-warning">
                <button data-dismiss="alert" class="close" type="button">&times;</button>
                Please make sure that your details below are correct and up to date before placing your booking. 
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Select Service for Appointment</h3>
                </div>
                <div class="panel-body" id="bookappointment">
                    <div class="col-md-12">
                        <h3 class="dark-grey">Choose your Service:</h3>
                        <div class="col-md-12" id="category_cont" >
                            <div class="form-group">
                                <label for="sel1">Category:</label>
                                <select class="form-control" name="category" id="category" >
                                    <option value=""></option>
                                    <?php foreach ($categories as $category) { ?>
                                        <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"><br>
                        </div>
                    </div>			
                    <div class="col-md-12" id="subcategory_cont" style="display: none;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="sel1">Sub Category:</label>
                                <select class="form-control" name="subcategory" id="subcategory">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"><br>
                        </div>
                    </div>	
                    <div class="col-md-12">
                        <div class="col-md-12" id="service_cont" style="display: none;">
                            <div class="form-group">
                                <label for="sel1">Service Name:</label>
                                <select class="form-control" name="service" id="service" >
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6"><br>

                        </div>
                        <br><br><br><br>
                        <button type="submit" class="btn btn-primary btn-lg btn-block" id="bookappointment_proceed_1">Proceed</button>
                    </div>	
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-rss"></i> Your Details</h3>
                </div>
                <div class="panel-body feed">
                    <section class="feed-item">
                        <div class="icon pull-left">
                            <i class="fa fa-comment"></i>
                        </div>
                        <div class="feed-item-body">
                            <div class="text">
                                <a href="#"><?php echo $client['full_name']; ?></a>
                            </div>

                        </div>
                    </section>
                    <section class="feed-item">
                        <div class="icon pull-left">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="feed-item-body">
                            <div class="text">
                                <a href="#"><?php echo $client['phone']; ?></a>
                            </div>

                        </div>
                    </section>
                    <section class="feed-item">
                        <div class="icon pull-left">
                            <i class="fa fa-plus-square-o"></i>
                        </div>
                        <div class="feed-item-body">
                            <div class="text">
                                <a href="#"><?php echo $client['email']; ?></a>
                            </div>

                        </div>
                    </section>
                    <section class="feed-item">
                        <div class="icon pull-left">
                            <i class="fa fa-archive"></i>
                        </div>
                        <div class="feed-item-body">
                            <div class="text">
                                <a href="#" id="stylist"><?php echo $client['stylist']; ?></a>
                            </div>
                        </div>
                    </section>


                </div>
            </div>
        </div>
    </div>
</div>

<div id="step2" style="display:none;">
    <div class="row">
        <div class="col-lg-12">
            <h1>Book Appointment</h1>
            <div class="alert alert-dismissable alert-warning">
                <button data-dismiss="alert" class="close" type="button">&times;</button>
                Please make sure that your details below are correct and up to date before placing your booking. 
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Select your Appointment Date</h3>
                </div> 
                <section class="feed-item">
                    <div class="feed-item-body">
                        <div class="alert alert-success">
                            When selecting your Day of choice you will be directed to the display of this days availability as well as two days after for your convenience.
                        </div>
                        <div id="datepicker_cont">
                            <label>Please select day</label>
                            <input type="text" id="datepicker" value="<?php echo date('Y-m-d'); ?>"/>
                        </div>
                    </div>
                </section> 
            </div>
            <div>
                <button type="submit" class="btn btn-primary btn-lg btn-block" id="bookappointment_back_1">Back to Choose your Service</button>
            </div>
        </div> 

        <div class="col-md-7" >
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Appointment Availability</h3>
                </div>
                <div  id="appointment_availability_content" >
                </div>
            </div>
        </div>


    </div>
</div>

<div id="step3" style="display:none;">
    <div class="row">
        <div class="col-lg-12">
            <h1>Appointment Confirmation</h1>
            <div class="alert alert-dismissable alert-warning">
                <button data-dismiss="alert" class="close" type="button">&times;</button>
                Please make sure that your details below are correct and up to date before placing your booking. 
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Appointment Confirmation details</h3>
                </div>
                <div class="panel-body">

                    <div class="col-md-6">
                        <section class="feed-item">
                            <div class="feed-item-body">
                                <div class="text" id="category_chosen">
                                    Category Chosen
                                </div>
                            </div>
                        </section>
                        <section class="feed-item">
                            <div class="feed-item-body">
                                <div class="text" id="subcategory_chosen">
                                    Sub Category Chosen
                                </div>
                            </div>
                        </section>
                        <section class="feed-item">
                            <div class="feed-item-body">
                                <div class="text" id="service_chosen">
                                    Service Name Chosen
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <section class="feed-item">
                            <div class="feed-item-body">
                                <div class="text" id="timeslot_chosen">
                                    Time Slot Chosen
                                </div>
                            </div>
                        </section>
                        <section class="feed-item">
                            <div class="feed-item-body">
                                <div class="text" id="stylist_chosen">
                                    Stylist Name
                                </div>
                            </div>
                        </section>
                        <section class="feed-item">
                            <div class="feed-item-body">
                                <div class="text">
                                    Time Required
                                </div>
                            </div>
                        </section>
                    </div>
                    <section class="feed-item">
                        <div class="col-md-12">
                            <br>
                            <button type="button" class="btn btn-primary btn-lg" id="confirm_appointment">Confirm and Place Booking</button>                          
                            <div id="bookappointment_message" class="alert" style="display: none;"></div>
                        </div></section>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-rss"></i> Previous Appointments</h3>
                </div>
                <div class="panel-body feed">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Service</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>29 June 2015</td>
                                <td>Upstyle</td>
                            </tr>
                            <tr>
                                <td>29 June 2015</td>
                                <td>Upstyle</td>
                            </tr>
                            <tr>
                                <td>29 June 2015</td>
                                <td>Upstyle</td>
                            </tr>
                            <tr>
                                <td>29 June 2015</td>
                                <td>Upstyle</td>
                            </tr>
                            <tr>
                                <td>29 June 2015</td>
                                <td>Upstyle</td>
                            </tr>
                            <tr>
                                <td>29 June 2015</td>
                                <td>Upstyle</td>
                            </tr>
                            <tr>
                                <td>29 June 2015</td>
                                <td>Upstyle</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

</div>
<!-- /#wrapper -->
</body>
</html>
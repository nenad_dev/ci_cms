<div class="row">
    <div class="col-lg-12">
        <h1> 
            <?php if ($own_bookings) { ?>
                Appointment History
            <?php } else { ?>
                New Bookings
            <?php } ?>
        </h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            <?php if ($own_bookings) { ?>
                View the list of all Appointment History
            <?php } else { ?>
                View the list of all New Bookings, Approve or Deny.
            <?php } ?>
            </h3></div>
    </div>
</div>
<?php if ($this->session->flashdata('error_message')) { ?>
    <div class="alert alert-danger">
        <?php echo $this->session->flashdata('error_message'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('success_message')) { ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success_message'); ?>
    </div>
<?php } ?>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i>
                    <?php if ($own_bookings) { ?>
                        List of Appointment History
                    <?php } else { ?>
                        List of New Bookings
                    <?php } ?>
                </h3></div>
            <div class="panel-body feed">
                <section class="feed-item">

                    <table class="table">
                        <thead>
                            <tr>
                                <?php if (!$own_bookings) { ?>
                                    <th>Full Name</th>
                                <?php } ?>
                                <th>Stylist</th>
                                <th>Service</th>
                                <th>Service Date</th>
                                <th>Service Time</th>
                                <th>Appointment Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($bookings as $booking) { ?>
                                <tr>
                                    <?php if (!$own_bookings) { ?>
                                        <td><?php echo $booking['client_full_name']; ?></td>
                                    <?php } ?>
                                    <td><?php echo $booking['stylist_full_name']; ?></td>
                                    <td><?php echo $booking['service_name']; ?></td>
                                    <td><?php echo $booking['appointment_start_date']; ?></td>
                                    <td><?php echo $booking['appointment_start_hour_min']; ?></td>
                                    <?php if ($own_bookings) { ?>
                                        <td><a href="<?php echo base_url(); ?>dashboard/appointment/<?php echo $booking['id']; ?>" >View Details</a></td>
                                    <?php } else { ?>
                                        <td><a href="<?php echo base_url(); ?>dashboard/bookingstatus/<?php echo $booking['id']; ?>" >View Details</a></td>
                                    <?php } ?>
                                </tr> 
                            <?php } ?>   
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
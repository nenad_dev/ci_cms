<section class="feed-item">
    <?php foreach ($stylist_calendar_items as $key => $value) { ?>
        <div class="col-md-2"> <h3><?php echo $key; ?></h3>                           
            <table class="table">
                <thead>
                    <tr>
                        <th>Time</th>
                        <th>Avaliability</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($value as $calendar_item_key => $calendar_item_value) { ?>
                        <tr>
                            <td><?php echo $calendar_item_key; ?></td>
                            <td class="taken"><?php echo $calendar_item_value; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table> </div>
    <?php } ?>
</section>
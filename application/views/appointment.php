<div class="row">
    <div class="col-lg-12">
        <h1>Appointment Details</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Appointment Details</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">
                    <div class="col-md-6">      
                        <div class="form-group col-lg-12">
                            <label>Full Name</label>
                            <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['client_full_name']; ?>" type="text"></fieldset>
                        </div>

                        <div class="form-group col-lg-12">
                            <label>Email Address</label>
                            <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['client_email']; ?>" type="text"></fieldset>
                        </div>

                        <div class="form-group col-lg-12">
                            <label>Contact Number</label>
                            <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['client_phone']; ?>" type="text"></fieldset>
                        </div>
                    </div>

                    <div class="col-md-6">      
                        <div class="form-group col-lg-12">
                            <label>Stylist</label>
                            <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['stylist_full_name']; ?>" type="text"></fieldset>
                        </div>

                        <div class="form-group col-lg-12">
                            <label>Service Chosen</label>
                            <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['service_name']; ?>" type="text"></fieldset>
                        </div>

                        <div class="form-group col-lg-12">
                            <label>Date</label>
                            <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['appointment_start_date']; ?>" type="text"></fieldset>
                        </div>

                        <div class="form-group col-lg-12">
                            <label>Time</label>
                            <fieldset disabled=""><input kl_virtual_keyboard_secure_input="on" class="form-control" value="<?php echo $booking['appointment_start_hour_min']; ?>" type="text"></fieldset>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
// if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        } else {
            redirect('/');
        }
    }

    public function register() {

        if ($this->session->userdata('logged_in') == 'yes') {
            redirect('/');
        }

// load models
        $this->load->model('user_model');

        $register['stylists'] = $this->user_model->get_users(3, '');
        $this->load->view('register', $register);
    }

    public function login() {
// load models
        $this->load->model('user_model');

// validate add profile form fields
        $this->form_validation->set_rules('username', 'Username', 'min_length[2]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'min_length[2]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message_login', 'Wrong username or password!');
            redirect('user/register');
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $login_success = $this->user_model->get_login_user($username, $password);
            if ($login_success) {
                redirect('/');
            } else {
                $this->session->set_flashdata('error_message_login', 'Wrong username or password!');
                redirect('user/register');
            }
        }
    }

    public function do_register() {

// load models
        $this->load->model('user_model');

// validate add profile form fields
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'min_length[2]|matches[confirm_password]|xss_clean');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'min_length[2]|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|min_length[2]|valid_email|required|xss_clean');
        $this->form_validation->set_rules('phone', 'Contact Number', 'trim|min_length[2]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message_register', validation_errors());
            redirect('user/register');
        } else {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $stylist_id = $this->input->post('stylist');
            $role = 4;

            $user_id = $this->user_model->set_add_user($first_name, $last_name, $password, $email, $phone, $role, $stylist_id);

            if ($user_id) {
                $user_full_name = $first_name . ' ' . $last_name;

                $this->session->set_userdata('user_id', $id);
                $this->session->set_userdata('user_full_name', $user_full_name);
                $this->session->set_userdata('logged_in', 'yes');
                $this->session->set_userdata('user_role', $role);

                redirect('/');
            } else {
                $this->session->set_flashdata('error_message_register', 'Some error appeared during registering!');
                redirect('user/register');
            }
        }
    }

    public function add_user() {

// if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        }

// load models
        $this->load->model('user_model');

// validate add profile form fields
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'min_length[2]|matches[confirm_password]|xss_clean');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'min_length[2]|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required|xss_clean');
        $this->form_validation->set_rules('phone', 'Contact Number', 'trim|min_length[2]|xss_clean');

        $creator_role = $this->session->userdata('user_role');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $role = $this->input->post('role');
            $stylist_id_post = $this->input->post('stylist');

// If stylist add client 
            if (($creator_role == 3) & ($role == 4)) {
                $stylist_id = $this->session->userdata('user_id');
            } elseif ($stylist_id_post) {
                $stylist_id = $stylist_id_post;
            } else {
                $stylist_id = '';
            }

            $user_id = $this->user_model->set_add_user($first_name, $last_name, $password, $email, $phone, $role, $stylist_id);
            // If user is stylist set work days
            if ($role == 3) {
                $mon_start_time = $this->input->post('mon_start_time');
                $mon_end_time = $this->input->post('mon_end_time');
                $tue_start_time = $this->input->post('tue_start_time');
                $tue_end_time = $this->input->post('tue_end_time');
                $wed_start_time = $this->input->post('wed_start_time');
                $wed_end_time = $this->input->post('wed_end_time');
                $thu_start_time = $this->input->post('thu_start_time');
                $thu_end_time = $this->input->post('thu_end_time');
                $fri_start_time = $this->input->post('fri_start_time');
                $fri_end_time = $this->input->post('fri_end_time');
                $sat_start_time = $this->input->post('sat_start_time');
                $sat_end_time = $this->input->post('sat_end_time');
                $sun_start_time = $this->input->post('sun_start_time');
                $sun_end_time = $this->input->post('sun_end_time');
                $this->user_model->set_insert_stylist_work_days($user_id, $mon_start_time, $mon_end_time, $tue_start_time, $tue_end_time, $wed_start_time, $wed_end_time, $thu_start_time, $thu_end_time, $fri_start_time, $fri_end_time, $sat_start_time, $sat_end_time, $sun_start_time, $sun_end_time);
            }

            if ($role == 2) {
                $this->session->set_flashdata('success_message', 'Admin successfully added!');
                redirect('dashboard/addadmin');
            } elseif ($role == 3) {
                $this->session->set_flashdata('success_message', 'Stylist successfully added!');
                redirect('dashboard/addstylist');
            } elseif ($role == 4) {
                $this->session->set_flashdata('success_message', 'Client successfully added!');
                redirect('dashboard/addclient');
            }
        }
    }

    public function edit_user() {

// if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        }

// load models
        $this->load->model('user_model');

// validate add profile form fields
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|min_length[2]|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|min_length[2]|required|xss_clean');
        $this->form_validation->set_rules('phone', 'Contact Number', 'trim|min_length[2]|xss_clean');

        $creator_role = $this->session->userdata('user_role');
        $id = $this->uri->segment(3);

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', validation_errors());
        } else {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $stylist_id_post = $this->input->post('stylist');
            $status_post = $this->input->post('status');

            $creator_role = $this->session->userdata('user_role');

            $own_profile = FALSE;
            if ($id == '') {
                $own_profile = TRUE;
                $id = $this->session->userdata('user_id');
            }

// Only Owner and Admin can approve client
            $status = '';
            if (($creator_role == 1) || ($creator_role == 2)) {
                $status = $status_post;
            }

// If stylist add client 
            $role = $this->user_model->get_user_role($id);
            if (($creator_role == 3) && ($role == 4)) {
                $stylist_id = $this->session->userdata('user_id');
            } elseif ($stylist_id_post) {
                $stylist_id = $stylist_id_post;
            } else {
                $stylist_id = '';
            }

            $this->user_model->set_edit_user($first_name, $last_name, $password, $email, $phone, $id, $status, $stylist_id);
            // If user is stylist set work days
            if ($role == 3) {
                $mon_start_time = $this->input->post('mon_start_time');
                $mon_end_time = $this->input->post('mon_end_time');
                $tue_start_time = $this->input->post('tue_start_time');
                $tue_end_time = $this->input->post('tue_end_time');
                $wed_start_time = $this->input->post('wed_start_time');
                $wed_end_time = $this->input->post('wed_end_time');
                $thu_start_time = $this->input->post('thu_start_time');
                $thu_end_time = $this->input->post('thu_end_time');
                $fri_start_time = $this->input->post('fri_start_time');
                $fri_end_time = $this->input->post('fri_end_time');
                $sat_start_time = $this->input->post('sat_start_time');
                $sat_end_time = $this->input->post('sat_end_time');
                $sun_start_time = $this->input->post('sun_start_time');
                $sun_end_time = $this->input->post('sun_end_time');
                $this->user_model->set_update_stylist_work_days($id, $mon_start_time, $mon_end_time, $tue_start_time, $tue_end_time, $wed_start_time, $wed_end_time, $thu_start_time, $thu_end_time, $fri_start_time, $fri_end_time, $sat_start_time, $sat_end_time, $sun_start_time, $sun_end_time);
            }
            $this->session->set_flashdata('success_message', 'Profile successfully updated!');
        }

        if ($own_profile) {
            redirect('user/edit');
        } else {
            redirect('user/edit/' . $id);
        }
    }

    public function delete() {

// if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        }

// allow deleting users only for owner
        if (($this->session->userdata('user_role') != 1)) {
            redirect('/');
        }

// load models
        $this->load->model('user_model');

        $id = $this->uri->segment(3);

        if ($this->user_model->get_user_has_clients($id)) {
            $this->session->set_flashdata('error_message', 'This Stylist can not be deleted! It has assigned clients!');
        } else {
            $response = $this->user_model->set_delete_user($id);
            if ($response) {
                $this->session->set_flashdata('success_message', 'User successfully deleted!');
            } else {
                $this->session->set_flashdata('error_message', 'Some error appeared!');
            }
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function logout() {
// if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        }

        $this->session->sess_destroy();
        redirect('user/register');
    }

    public function profile() {

// if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        }

// load models
        $this->load->model('user_model');
        $this->load->model('head_model');

// set variables needed for views
        $head['js_scripts'] = $this->head_model->set_head_scripts();
        $head['css'] = $this->head_model->set_head_css();
        $head['page_title'] = $this->head_model->set_head_page_title();
        $head['role'] = $this->session->userdata('user_role');

        $id = $this->uri->segment(3);
        $show_profile['profile'] = $this->user_model->get_profile_info($id);

// load views
        $this->load->view('header', $head);
        $this->load->view('show_profile', $show_profile);
    }

    public function edit() {

        // if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        }

        // load models
        $this->load->model('user_model');
        $this->load->model('head_model');

        // set variables needed for views
        $head['js_scripts'] = $this->head_model->set_head_scripts();
        $head['css'] = $this->head_model->set_head_css();
        $head['page_title'] = $this->head_model->set_head_page_title();
        $head['role'] = $this->session->userdata('user_role');

        $id = $this->uri->segment(3);

        // get user profile info
        $edit_profile['profile'] = $this->user_model->get_profile_info($id);
        
        // if user has not permissions for editing
        if ((empty($edit_profile['profile']) || (($head['role'] == 4) && ($id != ""))))
            redirect('/');

        if (($id && ($head['role'] != 4)) || ($id == '' && ($head['role'] == 4))) {
            $edit_profile['stylists'] = $this->user_model->get_users(3, '');
            if ($id == '')
                $id = $this->session->userdata('user_id');
            $edit_profile['stylist_id'] = $this->user_model->get_stylist_id($id);
        } else {
            $edit_profile['stylist_id'] = '';
        }

        // edit stylist work days
        $role = $this->session->userdata('user_role');
        if (($role == 1) && ($edit_profile['profile']['role'] == 3)) {

            $swd_info = $this->user_model->get_stylist_work_days($id);
            $stylist_work_days['swd_info'] = $swd_info;
            $edit_profile['stylist_work_days'] = $this->load->view('stylist_work_days', $stylist_work_days, TRUE);
        }

        // load views
        $this->load->view('header', $head);
        $this->load->view('editprofile', $edit_profile);
    }

    public function move_clients() {

        // if user is not logged in
        if ($this->session->userdata('logged_in') != 'yes') {
            redirect('user/register');
        }

        // allow moving clients only for owner
        if (($this->session->userdata('user_role') != 1)) {
            redirect('/');
        }

        // validate move clients form fields
        $this->form_validation->set_rules('from_stylist', 'From Stylist', 'numeric|required|xss_clean');
        $this->form_validation->set_rules('to_stylist', 'To Stylist', 'numeric|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_message', validation_errors());
        } else {
            $client = $this->input->post('client');
            $from_stylist = $this->input->post('from_stylist');
            $to_stylist = $this->input->post('to_stylist');

            // load models
            $this->load->model('user_model');

            $this->user_model->set_move_clients($client, $from_stylist, $to_stylist);
            if ($client == 'all') {
                $this->session->set_flashdata('success_message', 'Clients successfully moved!');
            } else {
                $this->session->set_flashdata('success_message', 'Client successfully moved!');
            }
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

}

<div class="row">
    <div class="col-lg-12">
        <h1>Client Dashboard</h1>
        <div class="alert alert-dismissable alert-warning">
            <button data-dismiss="alert" class="close" type="button">&times;</button>
            Welcome to your Profile Page! Customize your Profile, Book your appointments and view your previous visits. 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> Upcoming</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">
                    <div class="col-md-12"> <h3>Upcoming Bookings</h3>                           
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Stylist</th>
                                    <th>Service</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Appointment Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($upcoming_items as $upcoming_item) { ?>
                                    <tr>
                                        <td><?php echo $upcoming_item['stylist_full_name']; ?></td>
                                        <td><?php echo $upcoming_item['service_name']; ?></td>
                                        <td><?php echo $upcoming_item['appointment_start_date']; ?></td>
                                        <td><?php echo $upcoming_item['appointment_start_hour_min']; ?></td>
                                        <td><a class="btn btn-primary" role="button" href="bookstatus-details.php">Reshedule</a>&nbsp;<a class="btn btn-primary" role="button" href="bookstatus-details.php">Cancel</a></td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>    
                </section>
            </div>
        </div> 
    </div>     
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-rss"></i> History</h3></div>
            <div class="panel-body feed">
                <section class="feed-item">
                    <div class="col-md-12"> <h3>Previous Appointments</h3>                           
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Stylist</th>
                                    <th>Service</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Appointment Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($history_items as $history_item) { ?>
                                    <tr>
                                        <td><?php echo $history_item['stylist_full_name']; ?></td>
                                        <td><?php echo $history_item['service_name']; ?></td>
                                        <td><?php echo $history_item['appointment_start_date']; ?></td>
                                        <td><?php echo $history_item['appointment_start_hour_min']; ?></td>
                                        <td><a href="<?php echo base_url(); ?>dashboard/appointment/<?php echo $history_item['id']; ?>" >View Details</a></td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>    
                </section>
            </div>
        </div> 
    </div>
</div>
</div>
</div>
</div>
<!-- /#wrapper -->
</body>
</html>
